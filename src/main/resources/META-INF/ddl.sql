set FOREIGN_KEY_CHECKS=1;

CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `approvedEmail` bit(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `lockedUntil` datetime DEFAULT NULL,
  `passwordHash` varchar(255) NOT NULL,
  `registered` datetime DEFAULT NULL,
  
  `birthDate` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `workplace` varchar(255) DEFAULT NULL,
  
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_cs5bnaggwuluahrdh8mbs1rpe` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `administrator` (
	`id` bigint(20) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_ADMIN_ACCOUNT` FOREIGN KEY (`id`) REFERENCES `account`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Participants

CREATE TABLE `coach` (
  `qualification` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKgnv98ib8ommo3ng93mey5pmk0` FOREIGN KEY (`id`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `athlete` (
  `birthPlace` varchar(255) DEFAULT NULL,
  `children` int(11) NOT NULL,
  `diseases` varchar(255) DEFAULT NULL,
  `familyStatus` varchar(255) DEFAULT NULL,
  `height` float NOT NULL,
  `livingAddress` varchar(255) DEFAULT NULL,
  `livingPlace` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `parents` int(11) NOT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `sport` varchar(255) DEFAULT NULL,
  `sportDegree` varchar(255) DEFAULT NULL,
  `studyPlace` varchar(255) DEFAULT NULL,
  `weight` float NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK5rwipbbwkqmf2462o4io55wx5` FOREIGN KEY (`id`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `master` (
	`id` bigint(20) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Specialists

CREATE TABLE `specialist` (
  `academicDegree` varchar(255) DEFAULT NULL,
  `academicSpeciality` varchar(255) DEFAULT NULL,
  `academicTitle` varchar(255) DEFAULT NULL,
  `activityType` varchar(255) DEFAULT NULL,
  `highSpeciality` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKjkwhhikv2bh3jx6b4dlbsprfo` FOREIGN KEY (`id`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `psychologist` (
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK9f1bfefbfpqrbx7kj0ay68h23` FOREIGN KEY (`id`) REFERENCES `specialist` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `medic` (
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKnqw1jujprghygxi43phehvmlx` FOREIGN KEY (`id`) REFERENCES `specialist` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- specialists -> roles

CREATE TABLE `master_athletes` (
   `master_id` bigint(20) NOT NULL,
   `athlete_id` bigint(20) NOT NULL,
   KEY `FK_SPEC_ATHLETES_SPEC` (`master_id`),
   KEY `FK_SPEC_ATHLETES_AT` (`athlete_id`),
   CONSTRAINT `FK_SPEC_ATHLETES_SPEC` FOREIGN KEY (`master_id`) REFERENCES `master` (`id`) ON DELETE CASCADE,
   CONSTRAINT `FK_SPEC_ATHLETES_AT` FOREIGN KEY (`athlete_id`) REFERENCES `athlete` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- roles

CREATE TABLE `role` (
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `account_role` (
  `Account_id` bigint(20) NOT NULL,
  `roles_id` varchar(255) NOT NULL,
  KEY `FKthhwvtan9ejlmi7o14y8n9dmd` (`roles_id`),
  KEY `FKhk3yckm9oe7ht1uoot0inkcp6` (`Account_id`),
  CONSTRAINT `FKhk3yckm9oe7ht1uoot0inkcp6` FOREIGN KEY (`Account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKthhwvtan9ejlmi7o14y8n9dmd` FOREIGN KEY (`roles_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- enums

CREATE TABLE `enumtype` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `enumvalue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2s6e8e9bldttn3ihap63mn2cj` (`type_id`),
  CONSTRAINT `FK2s6e8e9bldttn3ihap63mn2cj` FOREIGN KEY (`type_id`) REFERENCES `enumtype` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- records

CREATE TABLE `recordgroup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` bigint(20) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sourceType` varchar(255) DEFAULT NULL,
  `periodicity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` bigint(20) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `valueType` varchar(255) DEFAULT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `enum_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKp5fjvbkl3uam4x9qpvkbxjvje` (`group_id`),
  CONSTRAINT `FKp5fjvbkl3uam4x9qpvkbxjvje` FOREIGN KEY (`group_id`) REFERENCES `recordgroup` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_record_enum` FOREIGN KEY(`enum_id`) REFERENCES `enumType` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- record values
CREATE TABLE `recordvalue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `athlete_id` bigint(20) DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `record_id` bigint(20) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `day` timestamp NULL DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKourf7yy2ppv50i7wttcekswc1` (`athlete_id`),
  KEY `FK3x728qwi1u5r251jq7d0ypqks` (`author_id`),
  KEY `FKwg9yoln6yudb112sj3t96vc6` (`record_id`),
  CONSTRAINT `FK3x728qwi1u5r251jq7d0ypqks` FOREIGN KEY (`author_id`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKourf7yy2ppv50i7wttcekswc1` FOREIGN KEY (`athlete_id`) REFERENCES `athlete` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKwg9yoln6yudb112sj3t96vc6` FOREIGN KEY (`record_id`) REFERENCES `record` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `integerrecordvalue` (
  `value` int(11) NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKlxjg5vw8ofq2qliq2wvipg80h` FOREIGN KEY (`id`) REFERENCES `recordvalue` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `floatrecordvalue` (
  `value` double NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKbgnkopxur306fquuf7kndq02h` FOREIGN KEY (`id`) REFERENCES `recordvalue` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `textrecordvalue` (
  `value` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK6lisnkhpmf7ldsj444kqe5cli` FOREIGN KEY (`id`) REFERENCES `recordvalue` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `enumrecordvalue` (
  `id` bigint(20) NOT NULL,
  `value_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK77n9x9l1kfwfjjx1ol30ija6y` (`value_id`),
  CONSTRAINT `FK77n9x9l1kfwfjjx1ol30ija6y` FOREIGN KEY (`value_id`) REFERENCES `enumvalue` (`id`) ON DELETE RESTRICT,
  CONSTRAINT `FKkitxyl0wrm4y5cclb0wfcmmvh` FOREIGN KEY (`id`) REFERENCES `recordvalue` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- athlete

CREATE TABLE `athlete_recordvalue` (
  `Athlete_id` bigint(20) NOT NULL,
  `records_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_6pw7dg557clnmlan9naolb9kb` (`records_id`),
  KEY `FKbeoddkqrw0pd5gp3o285vqrvq` (`Athlete_id`),
  CONSTRAINT `FKbeoddkqrw0pd5gp3o285vqrvq` FOREIGN KEY (`Athlete_id`) REFERENCES `athlete` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKghh6e8veh6bkded3h9ui9mylh` FOREIGN KEY (`records_id`) REFERENCES `recordvalue` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `athleterequest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `athlete_id` bigint(20) DEFAULT NULL,
  `master_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK75b6fol3h9updegbpadpg38je` (`athlete_id`),
  KEY `FKo7w7vjoh7wa6o8ys5am163uci` (`master_id`),
  CONSTRAINT `FK75b6fol3h9updegbpadpg38je` FOREIGN KEY (`athlete_id`) REFERENCES `athlete` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKo7w7vjoh7wa6o8ys5am163uci` FOREIGN KEY (`master_id`) REFERENCES `master` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `competitionresult` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `rank` int(11) NOT NULL,
  `athlete_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKnrm3pmin3gyqkwfnilv1ux5en` (`athlete_id`),
  CONSTRAINT `FKnrm3pmin3gyqkwfnilv1ux5en` FOREIGN KEY (`athlete_id`) REFERENCES `athlete` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- common

CREATE TABLE `message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isRead` bit(1) DEFAULT NULL,
  `sent` DATETIME DEFAULT NULL,
  `text` longtext,
  `receiver_id` bigint(20) DEFAULT NULL,
  `sender_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkvrff4tlkrsr8iatd45blty6` (`receiver_id`),
  KEY `FKeiuql0q8hqfw5s7fwodsomann` (`sender_id`),
  CONSTRAINT `FKeiuql0q8hqfw5s7fwodsomann` FOREIGN KEY (`sender_id`) REFERENCES `account` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FKkvrff4tlkrsr8iatd45blty6` FOREIGN KEY (`receiver_id`) REFERENCES `account` (`id`)  ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `dialogitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) DEFAULT NULL,
  `opposite_id` bigint(20) DEFAULT NULL,
  `is_receiver` bit(1) DEFAULT 0,
  `message_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_OWNERID` (`owner_id`),
  KEY `FK_OPPOSITEID` (`opposite_id`),
  KEY `FK_MESSAGEID` (`message_id`),
  CONSTRAINT `FK_OWNERID` FOREIGN KEY (`owner_id`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_OPPOSITEID` FOREIGN KEY (`opposite_id`) REFERENCES `account` (`id`)  ON DELETE CASCADE,
  CONSTRAINT `FK_MESSAGEID` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`)  ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `onetimetoken` (
  `id` varchar(255) NOT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKao9iu2djhveru0v9olg27ie02` (`account_id`),
  CONSTRAINT `FKao9iu2djhveru0v9olg27ie02` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;