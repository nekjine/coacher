window.convId = null;
window.conv = null;

var updater = {
    lastId: 0,
    start: function () {
        var self = this;
        setInterval(function () {
            self.update();
        }, 5000);
    },
    update: function (after) {
        var self = this;
        $.ajax({
            url: window.contextPath + "/rest/getMessages",
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data: JSON.stringify({
                idFrom: self.lastId,
                oppId: window.convId,
                limit: 100
            }),
            dataType: "json"
        }).done(function (result) {
            if (!result)
                return;
            result.messages.sort(function (a, b) {
                return a.id < b.id ? -1 : a.id == b.id ? 0 : 1;
            });
            if (result.messages.length > 0) {
                self.lastId = result.messages[result.messages.length - 1].id + 1;
                var toMarkAsRead = [];
                for (var msgKey in result.messages) {
                    var msg = result.messages[msgKey];
                    if (!msg.read && msg.senderId !== window.accId) {
                        toMarkAsRead.push(msg.id);
                    }
                    var textElement =
                            $("<span />", {
                                class: "text",
                                text: msg.text,
                            });
                    var authorElement =
                            $("<span />", {
                                class: "author " + (msg.senderId === window.accId ? "author_me" : "author_opposite"),
                                text: msg.senderId === window.accId ? "Вы" :
                                        window.conv["lastName"] + " " +
                                        (window.conv["firstName"] === null ? "" : (window.conv["firstName"].substring(0, 1) + ".")) +
                                        (window.conv["middleName"] === null ? "" : (window.conv["middleName"].substring(0, 1) + "."))
                            });
                    var messageElement =
                            $("<div />", {
                                class: "message"
                            });
                    messageElement.append(authorElement, ": ", textElement);
                    $("#chat_wrapper").append(messageElement);
                }
                $("#chat_wrapper").scrollTop($("#chat_wrapper").prop("scrollHeight"));
                _ajax("markMessagesRead", null, { ids: toMarkAsRead });
            }
            if (after != null)
                after();
        });
    }
};

$(function () {
    $.ajax({
        url: window.contextPath + "/rest/getAccount",
        method: "GET",
        headers: {
            'Accept': 'application/json'
        },
        data: {accountId: window.convId},
        dataType: "json"
    }).done(function (result) {
        window.conv = result;

        $("#send").on("click", function () {
            var text = $("#text").val();
            $.ajax({
                url: window.contextPath + "/rest/sendMessage",
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({receiverId: window.convId, text: text}),
                dataType: "json"
            });
            $("#text").val("");
        });

        updater.update(function () {
            updater.start();
        });
    });
});
