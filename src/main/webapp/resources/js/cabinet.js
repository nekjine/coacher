window.contextPath = "";

function _ajax(func, method, obj) {
    return $.ajax({
        url: window.contextPath + "/rest/" + func,
        method: method || "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(obj),
        dataType: "json"
    });
}

var unreadUpdater = {
    update : function() {
        _ajax("getUnreadMessagesCount", "GET", null).done(function(res) {
            $("#unread").text(res.result);
        });
    },
    start : function() {
        var self = this;
        setInterval(function(){
            self.update();
        }, 10000);
    }
};

$(function () {
    $('.date').datepicker({
        format: "dd.mm.yyyy",
        language: "ru"
    });
    unreadUpdater.start();
});