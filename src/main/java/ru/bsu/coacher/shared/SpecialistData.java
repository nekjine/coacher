package ru.bsu.coacher.shared;

public interface SpecialistData {
    String getActivityType();
    void setActivityType(String activityType);
    String getHighSpeciality();
    void setHighSpeciality(String highSpeciality);
    String getAcademicDegree();
    void setAcademicDegree(String academicDegree);
    String getAcademicSpeciality();
    void setAcademicSpeciality(String academicSpeciality);
    String getAcademicTitle();
    void setAcademicTitle(String academicTitle);
}
