package ru.bsu.coacher.shared;

import java.util.Date;

public interface AccountData {
    String getLastName();
    void setLastName(String lastName);
    String getFirstName();
    void setFirstName(String firstName);
    String getMiddleName();
    void setMiddleName(String middleName);
    Gender getGender();
    void setGender(Gender gender);
    Date getBirthDate();
    void setBirthDate(Date birthDate);
    String getWorkplace();
    void setWorkplace(String workplace);
    String getOccupation();
    void setOccupation(String occupation);
    String getPhone();
    void setPhone(String phone);
}
