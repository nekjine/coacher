
package ru.bsu.coacher.shared;

public class DefaultAthleteData implements AthleteData {
    private String birthPlace;
    private String nationality;
    private String religion;
    private String studyPlace;
    private float height;
    private float weight;
    private SportDegree sportDegree;
    private String sport;
    private int parents;
    private LivingPlace livingPlace;
    private String livingAddress;
    private FamilyStatus familyStatus;
    private int children;
    private String diseases;

    @Override
    public String getBirthPlace() {
        return birthPlace;
    }

    @Override
    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    @Override
    public String getNationality() {
        return nationality;
    }

    @Override
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String getReligion() {
        return religion;
    }

    @Override
    public void setReligion(String religion) {
        this.religion = religion;
    }

    @Override
    public String getStudyPlace() {
        return studyPlace;
    }

    @Override
    public void setStudyPlace(String studyPlace) {
        this.studyPlace = studyPlace;
    }

    @Override
    public float getHeight() {
        return height;
    }

    @Override
    public void setHeight(float height) {
        this.height = height;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public SportDegree getSportDegree() {
        return sportDegree;
    }

    @Override
    public void setSportDegree(SportDegree sportDegree) {
        this.sportDegree = sportDegree;
    }

    @Override
    public String getSport() {
        return sport;
    }

    @Override
    public void setSport(String sport) {
        this.sport = sport;
    }

    @Override
    public int getParents() {
        return parents;
    }

    @Override
    public void setParents(int parents) {
        this.parents = parents;
    }

    @Override
    public LivingPlace getLivingPlace() {
        return livingPlace;
    }

    @Override
    public void setLivingPlace(LivingPlace livingPlace) {
        this.livingPlace = livingPlace;
    }

    @Override
    public String getLivingAddress() {
        return livingAddress;
    }

    @Override
    public void setLivingAddress(String livingAddress) {
        this.livingAddress = livingAddress;
    }

    @Override
    public FamilyStatus getFamilyStatus() {
        return familyStatus;
    }

    @Override
    public void setFamilyStatus(FamilyStatus familyStatus) {
        this.familyStatus = familyStatus;
    }

    @Override
    public int getChildren() {
        return children;
    }

    @Override
    public void setChildren(int children) {
        this.children = children;
    }

    @Override
    public String getDiseases() {
        return diseases;
    }

    @Override
    public void setDiseases(String diseases) {
        this.diseases = diseases;
    }
    
    public SportDegree[] getAllSportDegrees() {
        return SportDegree.values();
    }
    
    public LivingPlace[] getAllLivingPlaces() {
        return LivingPlace.values();
    }
    
    public FamilyStatus[] getAllFamilyStatuses() {
        return FamilyStatus.values();
    }
}
