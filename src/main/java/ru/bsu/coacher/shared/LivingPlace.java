package ru.bsu.coacher.shared;

public enum LivingPlace {
    Dormitory("Общежитие"),
    Rent("Съемная квартира/комната"),
    Own("Собственная квартира/комната");
    private final String caption;
    public String getCaption() {
        return caption;
    }
    private LivingPlace(String caption) {
        this.caption = caption;
    }
}
