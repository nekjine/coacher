package ru.bsu.coacher.shared;

public class SpecialistUpdateData extends DefaultAccountData {
    private String activityType;
    private String highSpeciality;
    private String academicDegree;
    private String academicSpeciality;
    private String academicTitle;

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getHighSpeciality() {
        return highSpeciality;
    }

    public void setHighSpeciality(String highSpeciality) {
        this.highSpeciality = highSpeciality;
    }

    public String getAcademicDegree() {
        return academicDegree;
    }

    public void setAcademicDegree(String academicDegree) {
        this.academicDegree = academicDegree;
    }

    public String getAcademicSpeciality() {
        return academicSpeciality;
    }

    public void setAcademicSpeciality(String academicSpeciality) {
        this.academicSpeciality = academicSpeciality;
    }

    public String getAcademicTitle() {
        return academicTitle;
    }

    public void setAcademicTitle(String academicTitle) {
        this.academicTitle = academicTitle;
    }
}
