package ru.bsu.coacher.shared;

public interface AthleteData {
    String getBirthPlace();
    void setBirthPlace(String birthPlace);
    String getReligion();
    void setReligion(String religion);
    String getStudyPlace();
    void setStudyPlace(String studyPlace);
    float getHeight();
    void setHeight(float height);
    float getWeight();
    void setWeight(float weight);
    String getSport();
    void setSport(String sport);
    SportDegree getSportDegree();
    void setSportDegree(SportDegree sportDegree);
    int getParents();
    void setParents(int parents);
    LivingPlace getLivingPlace();
    void setLivingPlace(LivingPlace livingPlace);
    String getLivingAddress();
    void setLivingAddress(String livingAddress);
    FamilyStatus getFamilyStatus();
    void setFamilyStatus(FamilyStatus familyStatus);
    int getChildren();
    void setChildren(int children);
    String getDiseases();
    void setDiseases(String diseases);
    String getNationality();
    void setNationality(String nationality);
}
