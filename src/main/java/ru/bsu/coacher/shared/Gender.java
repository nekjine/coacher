package ru.bsu.coacher.shared;

public enum Gender {
    Male("Мужской"),
    Female("Женский");
    
    private final String label;

    public String getLabel() {
        return label;
    }
    
    private Gender(String label) {
        this.label = label;
    }
}
