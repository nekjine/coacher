package ru.bsu.coacher.shared;

public enum RecordSourceType {
    Athlete("Спортсмен"),
    Coach("Тренер"),
    Psychologist("Психолог"),
    Medic("Врач");
    private final String caption;
    private RecordSourceType(String caption) {
        this.caption = caption;
    }
    public String getCaption() {
        return caption;
    }
}
