package ru.bsu.coacher.web;

import java.io.IOException;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Administrator;
import ru.bsu.coacher.shared.PasswordHasher;

@Named
@RequestScoped
public class Login {

    private static final Logger log = LoggerFactory.getLogger(Login.class);

    @EJB
    private AccountManager accMgr;

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void loginAsCoach() {
        predefinedLogin("coach");
    }

    public void loginAsAthlete() {
        predefinedLogin("athlete");
    }

    private void predefinedLogin(String username) {
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
        WebUtils.getAndClearSavedRequest((ServletRequest) ctx.getRequest()); // silent clear
        this.username = username;
        this.password = "123";
        login();
    }

    public void login() {
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, PasswordHasher.hash(password));
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ctx = fctx.getExternalContext();
        try {
            subject.login(token);
            Account account = accMgr.findByEmail(username);
            subject.getSession().setAttribute("account", account);
            SavedRequest savedReq = WebUtils.getAndClearSavedRequest((ServletRequest) ctx.getRequest());
            try {
                if (account instanceof Administrator) {
                    ctx.redirect("admin/index.xhtml");
                } else {
                    ctx.redirect("cabinet/index.xhtml");
                }
            } catch (IOException ex) {
                log.error("An exception occured on redirect", ex);
            }
        } catch (AuthenticationException ex) {
            log.info("Login failed", ex);
            fctx.addMessage(null,
                new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Неверная пара логин/пароль.",
                    "Неверная пара логин/пароль."));
        }
    }

    public void logout() {
        SecurityUtils.getSubject().logout();
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ctx.invalidateSession();
            ctx.redirect("index.xhtml");
        } catch (IOException ex) {
            log.error("An exception occured on redirect", ex);
        }
    }
}
