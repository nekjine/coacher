package ru.bsu.coacher.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.bsu.coacher.eb.AccountManager;

@WebServlet("/w/*")
public class AppServlet extends HttpServlet {

    @EJB
    private AccountManager accMgr;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
        throws ServletException, IOException {
        
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        
        String contextPath = req.getContextPath();
        String path = req.getRequestURI().substring(contextPath.length());
        
        if ("/w/approve".equals(path)) {
            try {
                accMgr.approveByToken(req.getParameter("id"));
                resp.setStatus(200);
                try (PrintWriter writer = resp.getWriter()) {
                    writer.println("<!doctype html><html><head><meta charset='utf-8' />"
                        + "</head><body>Адрес электронной почты подтвержден.</body></html>");
                    writer.flush();
                }
            }
            catch (Exception ex) {
                resp.setStatus(404);
            }
        }
        else {
            resp.setStatus(404);
        }
    }
    
}
