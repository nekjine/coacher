package ru.bsu.coacher.web;

import java.io.IOException;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Administrator;

@Named
@RequestScoped
public class Index {
    @Inject
    private AccountBean accBean;
    
    public void check() throws IOException {
        Account account = accBean.getAccount();
        if (account != null) {
            if (account instanceof Administrator) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("admin/index.xhtml");
            } else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("cabinet/index.xhtml");
            }
        }
    }
}