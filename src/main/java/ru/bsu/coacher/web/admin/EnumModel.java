package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EnumModel implements Serializable {
    private final List<EnumValueModel> values = new ArrayList<>();
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EnumValueModel> getValues() {
        return values;
    }
    
    public void add() {
        values.add(new EnumValueModel());
    }
    
    public void remove(EnumValueModel obj) {
        values.remove(obj);
    }
}
