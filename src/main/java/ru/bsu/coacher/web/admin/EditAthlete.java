package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.eb.AthleteManager;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultAccountData;
import ru.bsu.coacher.shared.DefaultAthleteData;

@Named
@ViewScoped
public class EditAthlete implements Serializable {
    private long athleteId;
    private final DefaultAccountData accData = new DefaultAccountData();
    private final DefaultAthleteData athData = new DefaultAthleteData();
    
    @EJB
    private AthleteManager athleteMgr;
    
    @EJB
    private AccountManager accMgr;

    public DefaultAccountData getAccData() {
        return accData;
    }

    public DefaultAthleteData getAthData() {
        return athData;
    }

    public long getAthleteId() {
        return athleteId;
    }

    public void setAthleteId(long athleteId) {
        this.athleteId = athleteId;
        Athlete athlete = athleteMgr.findAthlete(athleteId);
        DataMapper.map(athlete, accData);
        DataMapper.map(athlete, athData);
    }
    
    public void updateData() { 
        athleteMgr.updateAthleteData(athleteId, athData);
        accMgr.updateAccountData(athleteId, accData);
        FacesContext.getCurrentInstance().addMessage(
                null, new FacesMessage("Изменения применены"));
    }
}
