package ru.bsu.coacher.web.admin;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.EnumTypeManager;
import ru.bsu.coacher.entities.EnumType;

@Named
@RequestScoped
public class EnumTypes {
    @EJB
    private EnumTypeManager recMgr;
    
    public List<EnumType> getRecords() {
        return recMgr.findAllEnumTypes();
    }
    
    public String delete(long enumTypeId) {
        recMgr.delete(enumTypeId);
        return "enums.xhtml?faces-redirect=true";
    }
}
