package ru.bsu.coacher.web.admin;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.EnumTypeManager;
import ru.bsu.coacher.eb.RecordManager;
import ru.bsu.coacher.entities.EnumType;
import ru.bsu.coacher.entities.records.RecordGroup;

@Named
@RequestScoped
public class RecordGroups {
    @EJB
    private RecordManager rgMgr;
    
    @EJB
    private EnumTypeManager enumTypeMgr;
    
    public List<RecordGroup> getRecordGroups() {
        return rgMgr.findAllRecordGroups();
    }
    
    public String deleteRecordGroup(long groupId) {
        rgMgr.deleteRecordGroup(groupId);
        return "record_groups.xhtml?faces-redirect=true";
    }
    
    public List<EnumType> getAllEnumTypes() {
        return enumTypeMgr.findAllEnumTypes();
    }
}
