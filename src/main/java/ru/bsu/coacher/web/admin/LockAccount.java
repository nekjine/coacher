package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;

@Named
@ViewScoped
public class LockAccount implements Serializable {

    private static Logger logger = Logger.getLogger(LockAccount.class.getName());
    
    private Date date;
    private Long accountId;

    @EJB
    private AccountManager accMgr;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public void lock() {
        if (date == null) {
            accMgr.unlock(accountId);
            FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage("Учетная запись разблокирована"));
        } else {
            accMgr.lock(accountId, date);
            FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage("Учетная запись заблокирована"));
        }
    }
}
