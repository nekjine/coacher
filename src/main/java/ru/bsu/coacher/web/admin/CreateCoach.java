package ru.bsu.coacher.web.admin;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.eb.CoachManager;
import ru.bsu.coacher.shared.DefaultCoachData;
import ru.bsu.coacher.shared.DefaultFullAccountData;

@Named
@RequestScoped
public class CreateCoach {
    
    private final DefaultFullAccountData accData = new DefaultFullAccountData();
    private final DefaultCoachData coachData = new DefaultCoachData();

    public DefaultFullAccountData getAccData() {
        return accData;
    }

    public DefaultCoachData getCoachData() {
        return coachData;
    }
    
    @EJB
    private AccountManager accMgr;
    @EJB
    private CoachManager coachMgr;
    
    public String create() {
        long id = coachMgr.createCoach(
            accData.getEmail(),
            accData.getPassword(), 
            coachData).getId();
        accMgr.updateAccountData(id, accData);
        return "accounts.xhtml?faces-redirect=true";
    }
}
