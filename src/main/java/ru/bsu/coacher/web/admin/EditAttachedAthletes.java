package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AthleteManager;
import ru.bsu.coacher.eb.MasterManager;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.Master;

@Named
@ViewScoped
public class EditAttachedAthletes implements Serializable {
    private long accId;
    private long athToAttachId;
    private List<Athlete> attachedAthletes;
    
    @EJB
    private AthleteManager athMgr;
    @EJB
    private MasterManager masterMgr;

    public long getAccId() {
        return accId;
    }

    public void setAccId(long accId) {
        this.accId = accId;
        Master master = masterMgr.find(accId);
        attachedAthletes = master.getAttachedAthletes();
    }

    public long getAthToAttachId() {
        return athToAttachId;
    }

    public void setAthToAttachId(long athToAttachId) {
        this.athToAttachId = athToAttachId;
    }
    
    public List<Athlete> getAttachedAthletes() {
        return attachedAthletes;
    }
    
    public List<Athlete> getAvailableAthletes() {
        List<Athlete> result = athMgr.findAllAthletes();
        result.removeAll(attachedAthletes);
        return result;
    }
    
    public void detach(long athId) {
        masterMgr.detachAthlete(accId, athId);
        attachedAthletes = masterMgr.find(accId).getAttachedAthletes();
    }
    public void attach() {
        masterMgr.attachAthlete(accId, athToAttachId);
        attachedAthletes = masterMgr.find(accId).getAttachedAthletes();
    }
    public void test(){
        
    }
}
