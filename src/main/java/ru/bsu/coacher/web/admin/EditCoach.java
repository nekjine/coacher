package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.eb.CoachManager;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultAccountData;
import ru.bsu.coacher.shared.DefaultCoachData;

@Named
@ViewScoped
public class EditCoach implements Serializable {
    
    private long coachId;
    private final DefaultAccountData accData = new DefaultAccountData();
    private final DefaultCoachData coachData = new DefaultCoachData();

    public DefaultAccountData getAccData() {
        return accData;
    }

    public DefaultCoachData getCoachData() {
        return coachData;
    }
    
    @EJB
    private AccountManager accMgr;
    @EJB
    private CoachManager coachMgr;

    public long getCoachId() {
        return coachId;
    }

    public void setCoachId(long coachId) {
        this.coachId = coachId;
        Coach coach = coachMgr.findCoach(coachId);
        DataMapper.map(coach, accData);
        DataMapper.map(coach, coachData);
    }
    
    public void updateData() {
        accMgr.updateAccountData(coachId, accData);
        coachMgr.updateCoachData(coachId, coachData);
        FacesContext.getCurrentInstance().addMessage(
                null, new FacesMessage("Изменения применены"));
    }
}
