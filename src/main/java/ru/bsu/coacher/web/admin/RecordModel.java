package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import ru.bsu.coacher.shared.RecordValueType;

public class RecordModel implements Serializable {
    private Long id;
    private Long enumTypeId;
    private String name;
    private String comment;
    private RecordValueType valueType;

    public Long getEnumTypeId() {
        return enumTypeId;
    }

    public void setEnumTypeId(Long enumTypeId) {
        this.enumTypeId = enumTypeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RecordValueType getValueType() {
        return valueType;
    }

    public void setValueType(RecordValueType valueType) {
        this.valueType = valueType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
}
