package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.RecordManager;
import ru.bsu.coacher.entities.records.Record;
import ru.bsu.coacher.entities.records.RecordGroup;

@Named
@ViewScoped
public class EditRecordGroup implements Serializable {
    private final RecordGroupModel model = new RecordGroupModel();
    
    private long recordGroupId;
    private boolean initialized;
    
    @EJB
    private RecordManager recMgr;

    public long getRecordGroupId() {
        return recordGroupId;
    }

    public RecordGroupModel getModel() {
        return model;
    }

    public void setRecordGroupId(long recordGroupId) {
        this.recordGroupId = recordGroupId;
    }
    
    public void init() {
        if (initialized)
            return;
        
        model.setId(recordGroupId);
        RecordGroup recordGroup = recMgr.findRecordGroup(recordGroupId);
        
        // view
        model.setName(recordGroup.getName());
        model.setComment(recordGroup.getComment());
        model.setPeriodicity(recordGroup.getPeriodicity());
        model.setSourceType(recordGroup.getSourceType());
        
        List<Record> list = recordGroup.getRecords();
        list.sort(new Comparator<Record>() {
            @Override
            public int compare(Record o1, Record o2) {
                return Long.compare(o1.getId(), o2.getId());
            }
        });
        
        for (Record record : list) {
            RecordModel recModel = new RecordModel();
            recModel.setId(record.getId());
            recModel.setName(record.getName());
            recModel.setComment(record.getComment());
            recModel.setValueType(record.getValueType());
            recModel.setEnumTypeId(record.getEnumType() == null ? null : record.getEnumType().getId());
            model.getRecords().add(recModel);
        }
        
        initialized = true;
    }
    
    public String edit() {
        RecordGroup recordGroup = recMgr.findRecordGroup(recordGroupId);
        recMgr.updateRecordGroup(
                recordGroupId, 
                model.getName(),
                model.getSourceType(), 
                model.getPeriodicity(), 
                model.getComment());
        
        // ids
        List<Long> recordGroupIds = new ArrayList<>();
        for (Record record : recordGroup.getRecords()) {
            recordGroupIds.add(record.getId());
        }
        
        List<Long> recordModelsIds = new ArrayList<>();
        for (RecordModel recModel : model.getRecords()) {
            if (recModel.getId() != null)
                recordModelsIds.add(recModel.getId());
        }
        
        // lists
        List<RecordModel> toAddList = new ArrayList<>();
        List<RecordModel> toUpdateList = new ArrayList<>();
        List<Long> toDeleteList = new ArrayList<>();
        
        List<RecordModel> models = model.getRecords();
        for (RecordModel recModel : models) {
            if (recModel.getId() == null) {
                toAddList.add(recModel);
            }
            else if (recordGroupIds.contains(recModel.getId())) {
                toUpdateList.add(recModel);
            }
        }
        for (Long recId : recordGroupIds) {
            if (!recordModelsIds.contains(recId)) {
                toDeleteList.add(recId);
            }
        }
        
        // lists processing
        for (RecordModel recModel : toAddList) {
            recMgr.addRecord(
                    recordGroupId,
                    recModel.getName(),
                    recModel.getValueType(),
                    recModel.getEnumTypeId(),
                    recModel.getComment());
        }
        for (RecordModel recModel : toUpdateList) {
            recMgr.updateRecord(
                    recModel.getId(), 
                    recModel.getName(),
                    recModel.getValueType(),
                    recModel.getEnumTypeId(),
                    recModel.getComment());
        }
        for (Long recordId : toDeleteList) {
            recMgr.deleteRecord(recordId);
        }
        return "record_groups.xhtml?faces-redirect=true";
    }
}
