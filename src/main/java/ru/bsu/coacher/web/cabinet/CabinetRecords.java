package ru.bsu.coacher.web.cabinet;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.eb.RecordManager;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.EnumValue;
import ru.bsu.coacher.entities.records.EnumRecordValue;
import ru.bsu.coacher.entities.records.FloatRecordValue;
import ru.bsu.coacher.entities.records.IntegerRecordValue;
import ru.bsu.coacher.entities.records.Record;
import ru.bsu.coacher.entities.records.RecordGroup;
import ru.bsu.coacher.entities.records.RecordValue;
import ru.bsu.coacher.entities.records.TextRecordValue;
import ru.bsu.coacher.shared.RecordValueType;
import ru.bsu.coacher.web.AccountBean;

@Named
@ViewScoped
public class CabinetRecords implements Serializable {
    private Long groupId;
    private boolean initialized;
    private Date date = new Date();
    
    private RecordGroupModel model;
    
    @EJB
    private RecordManager recMgr;
    
    @Inject
    private AccountBean accBean;

    public RecordGroupModel getModel() {
        return model;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public void reload() {
        initialized = false;
        init();
    }
    
    public void update() {
        final Account account = accBean.getAccount();
        model.getValueModels().stream().forEach(v -> {
            if (v.getValueId() != null) {
                final Long valueId = v.getValueId();
                switch (v.getValueType()) {
                    case Text:
                        if (v.getTextValue() == null)
                            recMgr.deleteRecordValue(valueId);
                        else
                            recMgr.updateTextRecordValue(valueId, v.getTextValue(), null);
                        break;
                    case Integer:
                        if (v.getIntValue() == null)
                            recMgr.deleteRecordValue(valueId);
                        else
                            recMgr.updateIntegerRecordValue(valueId, v.getIntValue(), null);
                        break;
                    case Float:
                        if (v.getFloatValue() == null)
                            recMgr.deleteRecordValue(valueId);
                        else
                            recMgr.updateFloatRecordValue(valueId, v.getFloatValue(), null);
                        break;
                    case Enum:
                        if (v.getEnumValueId() == null)
                            recMgr.deleteRecordValue(valueId);
                        else
                            recMgr.updateEnumRecordValue(valueId, v.getEnumValueId(), null);
                }
            } else {
                final Long recordId = v.getRecordId();
                switch (v.getValueType()) {
                    case Text:
                        if (v.getTextValue() != null)
                            recMgr.addTextRecordValue(account.getId(), recordId, date, v.getTextValue(), null);
                        break;
                    case Integer:
                        if (v.getIntValue() != null)
                            recMgr.addIntegerRecordValue(account.getId(), recordId, date, v.getIntValue(), null);
                        break;
                    case Float:
                        if (v.getFloatValue() != null)
                            recMgr.addFloatRecordValue(account.getId(), recordId, date, v.getFloatValue(), null);
                        break;
                    case Enum:
                        if (v.getEnumValueId() != null)
                            recMgr.addEnumRecordValue(account.getId(), recordId, date, v.getEnumValueId(), null);
                        break;
                }
            }
        });
    }
    
    public void init() {
        if (initialized)
            return;
        
        model = new RecordGroupModel();
        RecordGroup group = recMgr.findRecordGroup(groupId);
        
        List<Record> records = recMgr.findRecords(groupId);
        List<RecordValue> values = recMgr.findRecordValues(groupId, accBean.getAccount().getId(), date);
        
        model.setId(group.getId());
        model.setName(group.getName());
        model.setPeriodicity(group.getPeriodicity());
        model.setSourceType(group.getSourceType());
        
        records.stream().forEach((rec) -> {
            RecordValueModel rvModel = new RecordValueModel();
            final long recId = rec.getId();
            Optional<RecordValue> exgOptValue =
                values
                .stream()
                .filter(v -> v.getRecord().getId() == recId)
                .findFirst();
            
            rvModel.setRecordId(rec.getId());
            rvModel.setName(rec.getName());
            rvModel.setValueType(rec.getValueType());
            if (exgOptValue.isPresent()) {
                RecordValue exgValue = exgOptValue.get();
                rvModel.setValueId(exgValue.getId());
                rvModel.setAdded(exgValue.getAdded());
                
                rvModel.setUpdated(exgValue.getUpdated());
                switch (rec.getValueType()) {
                    case Text: 
                        rvModel.setTextValue(((TextRecordValue)exgValue).getValue());
                        rvModel.setHasValue(true);
                        break;
                    case Integer:
                        rvModel.setIntValue(((IntegerRecordValue)exgValue).getValue());
                        rvModel.setHasValue(true);
                        break;
                    case Float:
                        rvModel.setFloatValue(((FloatRecordValue)exgValue).getValue());
                        rvModel.setHasValue(true);
                        break;
                    case Enum:
                        EnumValue enumValue = ((EnumRecordValue)exgValue).getValue();
                        rvModel.setEnumValueId(enumValue.getId());
                        rvModel.setEnumValue(enumValue);
                        rvModel.setHasValue(true);
                        break;
                }
            }
            if (rec.getValueType() == RecordValueType.Enum &&
                rec.getEnumType() != null)
                rvModel.setEnumValues(rec.getEnumType().getValues());
            model.getValueModels().add(rvModel);
        });
        initialized = true;
    }
}
