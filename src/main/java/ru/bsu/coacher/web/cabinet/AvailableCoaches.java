package ru.bsu.coacher.web.cabinet;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.eb.AthleteManager;
import ru.bsu.coacher.eb.CoachManager;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.web.AccountBean;

@Named
@RequestScoped
public class AvailableCoaches {
    @EJB
    private AthleteManager athMgr;
    
    @EJB
    private CoachManager coachMgr;
    
    @Inject
    private AccountBean accBean;
    
    public boolean canMakeRequests() {
        Account account = accBean.getAccount();
        if (account instanceof Athlete == false) {
            return false;
        }
        Athlete athlete = (Athlete)accBean.getActualAccount();
        return
            athMgr.findAthleteRequest(athlete.getId()) == null &&
            athlete.getCoach() == null;
    }
    
    public boolean hasRequests() {
        Account account = accBean.getAccount();
        if (account instanceof Athlete == false) {
            return false;
        }
        Athlete athlete = (Athlete)accBean.getActualAccount();
        return athMgr.findAthleteRequest(athlete.getId()) != null;
    }
    
    public boolean hasCoach() {
        Account account = accBean.getActualAccount();
        if (account instanceof Athlete == false) {
            return false;
        }
        Athlete athlete = (Athlete)account;
        return athlete.getCoach() != null;
    }
    
    public List<Coach> getAvailableCoaches() {
        if (canMakeRequests())
            return coachMgr.findAllCoaches();
        return new ArrayList<>();
    }
    
    public String createRequest(Long coachId) {
        Athlete athlete = (Athlete)accBean.getActualAccount();
        if (
            athMgr.findAthleteRequest(athlete.getId()) != null ||
            athlete.getCoach() != null) {
            throw new IllegalStateException();
        }
        athMgr.createAthleteRequest(athlete.getId(), coachId);
        return "pending_requests.xhtml?faces-redirect=true";
    }
}