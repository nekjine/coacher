package ru.bsu.coacher.web.cabinet;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.Master;
import ru.bsu.coacher.web.AccountBean;

@Named
@RequestScoped
public class CabinetAthletes {
    @Inject
    private AccountBean accBean;
    public List<Athlete> getAthletes() {
        return ((Master)accBean.getActualAccount()).getAttachedAthletes();
    }
}
