package ru.bsu.coacher.web.cabinet;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import ru.bsu.coacher.entities.EnumValue;
import ru.bsu.coacher.shared.RecordValueType;

public class RecordValueModel implements Serializable {
    private String name;
    private Integer intValue;
    private Float floatValue;
    private Long enumValueId;
    private Long valueId;
    private Long recordId;
    private String textValue;
    private RecordValueType valueType;
    private EnumValue enumValue;
    private Collection<EnumValue> enumValues;
    private boolean hasValue;
    private Date added;
    private Date updated;

    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public boolean isHasValue() {
        return hasValue;
    }

    public void setHasValue(boolean hasValue) {
        this.hasValue = hasValue;
    }

    public EnumValue getEnumValue() {
        return enumValue;
    }

    public void setEnumValue(EnumValue enumValue) {
        this.enumValue = enumValue;
    }

    public Collection<EnumValue> getEnumValues() {
        return enumValues;
    }

    public void setEnumValues(Collection<EnumValue> enumValues) {
        this.enumValues = enumValues;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIntValue() {
        return intValue;
    }

    public void setIntValue(Integer intValue) {
        this.intValue = intValue;
    }

    public Float getFloatValue() {
        return floatValue;
    }

    public void setFloatValue(Float floatValue) {
        this.floatValue = floatValue;
    }

    public Long getEnumValueId() {
        return enumValueId;
    }

    public void setEnumValueId(Long enumValueId) {
        this.enumValueId = enumValueId;
    }

    public Long getValueId() {
        return valueId;
    }

    public void setValueId(Long valueId) {
        this.valueId = valueId;
    }

    public RecordValueType getValueType() {
        return valueType;
    }

    public void setValueType(RecordValueType valueType) {
        this.valueType = valueType;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }
}
