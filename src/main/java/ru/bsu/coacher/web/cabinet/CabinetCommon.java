package ru.bsu.coacher.web.cabinet;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.web.AccountBean;

@Named
@RequestScoped
public class CabinetCommon {
    @Inject
    private AccountBean accBean;
    
    public Integer getIncomingRequestsCount() {
        return accBean.<Coach>getTypedActualAccount().getRequests().size();
    }
}
