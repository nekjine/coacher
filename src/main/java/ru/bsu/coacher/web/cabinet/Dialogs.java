package ru.bsu.coacher.web.cabinet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.eb.MasterManager;
import ru.bsu.coacher.eb.MessageManager;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.Master;
import ru.bsu.coacher.shared.LastMessage;
import ru.bsu.coacher.web.AccountBean;

@Named
@RequestScoped
public class Dialogs {
    
    @EJB
    MessageManager msgMgr;
    
    @EJB
    MasterManager masterMgr;
    
    @Inject
    AccountBean accBean;
    
    private long participantId;

    public long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(long participantId) {
        this.participantId = participantId;
    }
    
    public String goToDialogView() {
        return "dialog.xhtml?faces-redirect=true&id=" + participantId;
    }
    
    private <T, F extends T> List<T> convert(List<F> inList) {
        ArrayList<T> outList = new ArrayList<>(inList.size());
        for (F e : inList) {
            outList.add((T)e);
        }
        return outList;
    }
    
    public Long getUnreadCount() {
        return msgMgr.getNonReadCount(accBean.getAccount().getId());
    }
    
    public Collection<Account> getAvailableReceivers() {
        Account account = accBean.getActualAccount();
        
        if (account instanceof Master) {
            Master master = (Master)account;
            return this.<Account, Athlete>convert(master.getAttachedAthletes());
        } else if (account instanceof Athlete) {
            Athlete athlete = (Athlete)account;
            List<Account> result = this.<Account, Master>convert(athlete.getMasters());
            return result;
        }
        return new ArrayList<>();
    }
    
    public Collection<LastMessage> getLastMessages() {
        return msgMgr.findLastMessages(accBean.getAccount().getId());
    }
}