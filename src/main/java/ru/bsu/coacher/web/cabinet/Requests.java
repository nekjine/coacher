package ru.bsu.coacher.web.cabinet;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.eb.MasterManager;
import ru.bsu.coacher.entities.AthleteRequest;
import ru.bsu.coacher.web.AccountBean;

@Named
@RequestScoped
public class Requests {
    @Inject
    private AccountBean accBean;
    
    @EJB
    private MasterManager masterMgr;
    
    public List<AthleteRequest> getRequests() {
        return masterMgr.findAthleteRequests(
            accBean.getActualAccount().getId());
    }
    
    public void accept(Long requestId) {
        masterMgr.acceptAthleteRequest(requestId);
    }
    
    public void decline(Long requestId) {
        masterMgr.declineAthleteRequest(requestId);
    }
}