package ru.bsu.coacher.web;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;

@Named @RequestScoped
public class ChangePassword {
    private String oldPassword;
    private String newPassword;
    private String newPasswordAgain;
    
    @Inject
    private AccountBean accBean;
    @EJB
    private AccountManager accMgr;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordAgain() {
        return newPasswordAgain;
    }

    public void setNewPasswordAgain(String newPasswordAgain) {
        this.newPasswordAgain = newPasswordAgain;
    }
    
    public void change() {
        if (!newPassword.equals(newPasswordAgain)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Пароли не совпадают"));
            return;
        }
        try {
            accMgr.changePassword(accBean.getAccount().getId(), oldPassword, newPassword);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Пароль успешно изменен"));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Пароль введен неверно"));
        }
    }
}