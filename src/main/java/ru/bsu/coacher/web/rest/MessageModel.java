package ru.bsu.coacher.web.rest;

import ru.bsu.coacher.entities.Message;

public class MessageModel {
    private Long id;
    private Long date;
    private String text;
    private Long senderId;
    private Long receiverId;
    private Boolean read;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
    

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }
    
    public MessageModel fromMessage(Message message) {
        id = message.getId();
        date = message.getSent().getTime();
        text = message.getText();
        senderId = message.getSender().getId();
        receiverId = message.getReceiver().getId();
        read = message.getIsRead();
        return this;
    }
}
