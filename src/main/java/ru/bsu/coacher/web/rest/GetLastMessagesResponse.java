package ru.bsu.coacher.web.rest;

import ru.bsu.coacher.shared.LastMessage;

public class GetLastMessagesResponse extends BasicResponse {
    public LastMessage[] result;

    public LastMessage[] getResult() {
        return result;
    }

    public void setResult(LastMessage[] result) {
        this.result = result;
    }
}
