package ru.bsu.coacher.web.rest;

import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.entities.Medic;
import ru.bsu.coacher.entities.Psychologist;

public class AccountModel {
    private long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String role;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    public AccountModel fromAccount(Account account) {
        lastName = account.getLastName();
        firstName = account.getFirstName();
        middleName = account.getMiddleName();
        if (account instanceof Athlete) {
            role = "athlete";
        } else if (account instanceof Coach) {
            role = "coach";
        } else if (account instanceof Medic) {
            role = "medic";
        } else if (account instanceof Psychologist) {
            role = "psychologist";
        }
        id = account.getId();
        return this;
    }
}
