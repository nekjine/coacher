package ru.bsu.coacher.web.rest;

public class LoginRequest {
    private String userName;
    private String passHash;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassHash() {
        return passHash;
    }

    public void setPassHash(String passHash) {
        this.passHash = passHash;
    }
    
}
