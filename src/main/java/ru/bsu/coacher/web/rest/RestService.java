package ru.bsu.coacher.web.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.eb.AthleteManager;
import ru.bsu.coacher.eb.CoachManager;
import ru.bsu.coacher.eb.Mailer;
import ru.bsu.coacher.eb.MasterManager;
import ru.bsu.coacher.eb.MessageManager;
import ru.bsu.coacher.eb.RecordManager;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.AthleteRequest;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.entities.DialogItem;
import ru.bsu.coacher.entities.records.EnumRecordValue;
import ru.bsu.coacher.entities.records.FloatRecordValue;
import ru.bsu.coacher.entities.records.IntegerRecordValue;
import ru.bsu.coacher.entities.records.RecordGroup;
import ru.bsu.coacher.entities.records.RecordValue;
import ru.bsu.coacher.entities.records.TextRecordValue;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultAccountData;
import ru.bsu.coacher.shared.DefaultAthleteData;
import ru.bsu.coacher.shared.DefaultCoachData;
import ru.bsu.coacher.shared.LastMessage;
import ru.bsu.coacher.shared.PasswordHasher;

@Path("/")
public class RestService {

    private static final Logger logger = Logger.getLogger(RestService.class.getName());

    @EJB
    private AccountManager accMgr;

    @EJB
    private MessageManager msgMgr;

    @EJB
    private RecordManager recMgr;

    @EJB
    private AthleteManager athMgr;

    @EJB
    private CoachManager coachMgr;

    @EJB
    private MasterManager masterMgr;
    
    @EJB
    private Mailer mailer;

    private Account getAccount() {
        return (Account) SecurityUtils.getSubject().getSession().getAttribute("account");
    }

    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public LoginResponse login(LoginRequest params) {
        LoginResponse response = new LoginResponse();
        try {
            Subject subject = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(params.getUserName(), PasswordHasher.hash(params.getPassHash()));
            subject.login(token);
            Account account = accMgr.findByEmail(params.getUserName());
            subject.getSession().setAttribute("account", account);
            response.setMainRole(account.hasRole("athlete") ? "athlete" : "coach");
            response.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            response.setSuccess(false);
        }
        return response;
    }

    @POST
    @Path("logout")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse logout() {
        BasicResponse response = new BasicResponse();
        try {
            Subject subject = SecurityUtils.getSubject();
            subject.logout();
            response.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            response.setSuccess(false);
        }
        return response;
    }

    @GET
    @Path("probe")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse probe() {
        BasicResponse response = new BasicResponse();
        Subject subject = SecurityUtils.getSubject();
        response.setSuccess(subject.isAuthenticated());
        return response;
    }

    @POST
    @Path("sendMessage")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse sendMessage(SendMessageParameters params) {
        BasicResponse response = new BasicResponse();
        try {
            msgMgr.sendMessage(
                getAccount().getId(),
                params.getReceiverId(),
                params.getText());
            response.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            response.setSuccess(false);
            response.setErrorMessage("Ошибка отправки сообщения");
        }
        return response;
    }

    @POST
    @Path("getMessages")
    @Produces(MediaType.APPLICATION_JSON)
    public GetMessagesResponse getMessages(GetMessagesRequest params) {
        GetMessagesResponse response = new GetMessagesResponse();
        try {
            List<DialogItem> items = msgMgr.findDialogItems(
                getAccount().getId(),
                params.getOppId(),
                params.getIdFrom(),
                params.getLimit(),
                params.getDateFrom(),
                params.getDateTo());
            List<MessageModel> resultList = new ArrayList<>();
            for (DialogItem item : items) {
                MessageModel model = new MessageModel();
                model.setId(item.getMessage().getId());
                model.setDate(item.getMessage().getSent().getTime());
                model.setSenderId(item.getMessage().getSender().getId());
                model.setReceiverId(item.getMessage().getReceiver().getId());
                model.setText(item.getMessage().getText());
                model.setRead(item.getMessage().getIsRead());
                resultList.add(model);
            }
            response.setMessages(resultList);
            response.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            response.setSuccess(false);
            response.setErrorMessage(ex.getMessage());
        }
        return response;
    }

    @POST
    @Path("markMessagesRead")
    @Produces(MediaType.APPLICATION_JSON)
    public IntegerResponse markMessagesAsRead(MarkAsReadRequest params) {
        IntegerResponse response = new IntegerResponse();
        try {
            int affected = msgMgr.markAsRead(getAccount().getId(), params.getIds());
            response.setResult(affected);
            response.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            response.setSuccess(false);
            response.setErrorMessage(ex.getMessage());
        }
        return response;
    }

    @POST
    @Path("markAllMessagesRead")
    @Produces(MediaType.APPLICATION_JSON)
    public IntegerResponse markAllMessagesAsRead() {
        IntegerResponse response = new IntegerResponse();
        try {
            Integer result = msgMgr.markAllAsRead(getAccount().getId());
            response.setResult(result);
            response.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            response.setSuccess(false);
            response.setErrorMessage("Ошибка отправки сообщения");
        }
        return response;
    }

    @GET
    @Path("getUnreadMessagesCount")
    @Produces(MediaType.APPLICATION_JSON)
    public IntegerResponse getUnreadMessagesCount() {
        IntegerResponse response = new IntegerResponse();
        try {
            Integer result = msgMgr.getNonReadCount(getAccount().getId()).intValue();
            response.setResult(result);
            response.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            response.setSuccess(false);
            response.setErrorMessage("Ошибка отправки сообщения");
        }
        return response;
    }

    @GET
    @Path("getLastMessages")
    @Produces(MediaType.APPLICATION_JSON)
    public GetLastMessagesResponse getLastMessages() {
        GetLastMessagesResponse response = new GetLastMessagesResponse();
        try {
            List<LastMessage> lastMessages = msgMgr.findLastMessages(getAccount().getId());
            response.setResult(lastMessages.toArray(new LastMessage[lastMessages.size()]));
            response.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            response.setSuccess(false);
            response.setErrorMessage("Ошибка отправки сообщения");
        }
        return response;
    }

    @GET
    @Path("getAccount")
    @Produces(MediaType.APPLICATION_JSON)
    public AccountModel getAccount(@QueryParam("accountId") Long accountId) {
        if (accountId == null) {
            throw new IllegalArgumentException();
        }
        boolean exists = false;
        for (Account account
            : msgMgr.getAvailableReceivers(getAccount().getId())) {
            if (Objects.equals(account.getId(), accountId)) {
                exists = true;
            }
        }
        if (!exists) {
            throw new IllegalStateException();
        }

        Account acc = accMgr.find(accountId);
        return new AccountModel().fromAccount(acc);
    }

    @GET
    @Path("getCommonAthleteRecords")
    @Produces(MediaType.APPLICATION_JSON)
    public GetCommonAthleteRecordsResponse getCommonAthleteRecords(
        @QueryParam("athleteId") Long athleteId,
        @QueryParam("day") String day) {
        Account account = getAccount();
        if (account instanceof Athlete) {
            if (athleteId == null) {
                athleteId = account.getId();
            } else if (!Objects.equals(athleteId, account.getId())) {
                throw new IllegalStateException();
            }
        } else if (account instanceof Coach) {
            if (athleteId == null) {
                throw new IllegalStateException();
            }
            Coach coach = coachMgr.findCoach(account.getId());
            final Long accId = athleteId;
            if (!coach.getAttachedAthletes().stream().anyMatch(a -> a.getId().equals(accId))) {
                throw new IllegalStateException();
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        Date dayDate = null;
        try {
            dayDate = sdf.parse(day);
        } catch (ParseException ex) {
            throw new RuntimeException(ex);
        }
        RecordGroup group = recMgr.findRecordGroupByCode(0);
        List<RecordValue> values = recMgr.findRecordValues(group.getId(), athleteId, dayDate);
        CommonAthleteRecords records = new CommonAthleteRecords();

        values.stream().forEach((item) -> {
            int code = item.getRecord().getCode();
            IntegerRecordValue intValue = null;
            EnumRecordValue enumValue = null;
            FloatRecordValue floatValue = null;
            TextRecordValue textValue = null;
            if (item instanceof IntegerRecordValue) {
                intValue = (IntegerRecordValue) item;
            } else if (item instanceof EnumRecordValue) {
                enumValue = (EnumRecordValue) item;
            } else if (item instanceof FloatRecordValue) {
                floatValue = (FloatRecordValue) item;
            } else if (item instanceof TextRecordValue) {
                textValue = (TextRecordValue) item;
            }
            switch (code) {
                case 0:
                    records.setMorningLyingPulseRate(intValue.getValue());
                    break;
                case 1:
                    records.setMorningStandingPulseRate(intValue.getValue());
                    break;
                case 2:
                    records.setDiastolicBloodPressure(intValue.getValue());
                    break;
                case 3:
                    records.setSystolicBloodPressure(intValue.getValue());
                    break;
                case 4:
                    records.setSleepValueCode(enumValue.getValue().getCode());
                    break;
                case 5:
                    records.setComplainsValueCode(enumValue.getValue().getCode());
                    break;
                case 6:
                    records.setMoodValueCode(enumValue.getValue().getCode());
                    break;
                case 7:
                    records.setFeelingValueCode(enumValue.getValue().getCode());
                    break;
                case 8:
                    records.setOperabilityValueCode(enumValue.getValue().getCode());
                    break;
                case 9:
                    records.setAppetiteValueCode(enumValue.getValue().getCode());
                    break;
                case 10:
                    records.setBowelFunctionValueCode(enumValue.getValue().getCode());
                    break;
                case 11:
                    records.setTrainingMoodValueCode(enumValue.getValue().getCode());
                    break;
                case 12:
                    records.setBeforeTrainingWeight(floatValue.getValue());
                    break;
                case 13:
                    records.setBeforeTrainingPulseRate(intValue.getValue());
                    break;
                case 14:
                    records.setTrainingRegimeValueCode(enumValue.getValue().getCode());
                    break;
                case 15:
                    records.setTrainingPlan(textValue.getValue());
                    break;
                case 16:
                    records.setAfterTrainingPulseRate(intValue.getValue());
                    break;
                case 17:
                    records.setAfterTrainingWeight(floatValue.getValue());
                    break;
                case 18:
                    records.setSweatingCodeId(enumValue.getValue().getCode());
                    break;
                case 19:
                    records.setLoadHandlingCodeId(enumValue.getValue().getCode());
                    break;
                case 20:
                    records.setSportFood(textValue.getValue());
                    break;
                case 21:
                    records.setDrugs(textValue.getValue());
                    break;
            }
        });

        GetCommonAthleteRecordsResponse result = new GetCommonAthleteRecordsResponse();
        result.setRecords(records);
        result.setSuccess(true);
        return result;
    }

    @POST
    @Path("postCommonAthleteRecords")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse postCommonAthleteRecords(PostCommonAthleteRecordsRequest params) {
        BasicResponse result = new BasicResponse();
        try {
            CommonAthleteRecords rec = params.getRecords();
            Account acc = getAccount();

            if (rec.getMorningLyingPulseRate() != null) {
                recMgr.addOrUpdateIntegerRecordValue(
                    acc.getId(), acc.getId(),
                    0, 0,
                    rec.getMorningLyingPulseRate(),
                    params.getDay(),
                    null);
            }
            if (rec.getMorningStandingPulseRate() != null) {
                recMgr.addOrUpdateIntegerRecordValue(
                    acc.getId(), acc.getId(),
                    0, 1,
                    rec.getMorningStandingPulseRate(),
                    params.getDay(),
                    null);
            }
            if (rec.getDiastolicBloodPressure() != null) {
                recMgr.addOrUpdateIntegerRecordValue(
                    acc.getId(), acc.getId(),
                    0, 2,
                    rec.getDiastolicBloodPressure(),
                    params.getDay(),
                    null);
            }
            if (rec.getSystolicBloodPressure() != null) {
                recMgr.addOrUpdateIntegerRecordValue(
                    acc.getId(), acc.getId(),
                    0, 3,
                    rec.getSystolicBloodPressure(),
                    params.getDay(),
                    null);
            }
            if (rec.getSleepValueCode() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 4,
                    0, rec.getSleepValueCode(),
                    params.getDay(),
                    null);
            }
            if (rec.getComplainsValueCode() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 5,
                    1, rec.getComplainsValueCode(),
                    params.getDay(),
                    null);
            }
            if (rec.getMoodValueCode() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 6,
                    0, rec.getMoodValueCode(),
                    params.getDay(),
                    null);
            }
            if (rec.getFeelingValueCode() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 7,
                    0, rec.getFeelingValueCode(),
                    params.getDay(),
                    null);
            }
            if (rec.getOperabilityValueCode() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 8,
                    2, rec.getOperabilityValueCode(),
                    params.getDay(),
                    null);
            }
            if (rec.getAppetiteValueCode() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 9,
                    0, rec.getAppetiteValueCode(),
                    params.getDay(),
                    null);
            }
            if (rec.getBowelFunctionValueCode() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 10,
                    3, rec.getBowelFunctionValueCode(),
                    params.getDay(),
                    null);
            }
            if (rec.getTrainingMoodValueCode() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 11,
                    4, rec.getTrainingMoodValueCode(),
                    params.getDay(),
                    null);
            }
            if (rec.getBeforeTrainingWeight() != null) {
                recMgr.addOrUpdateFloatRecordValue(
                    acc.getId(), acc.getId(),
                    0, 12,
                    rec.getBeforeTrainingWeight(), params.getDay(), null);
            }
            if (rec.getBeforeTrainingPulseRate() != null) {
                recMgr.addOrUpdateIntegerRecordValue(
                    acc.getId(), acc.getId(),
                    0, 13,
                    rec.getBeforeTrainingPulseRate(), params.getDay(), null);
            }
            if (rec.getTrainingRegimeValueCode() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 14,
                    5, rec.getTrainingRegimeValueCode(),
                    params.getDay(),
                    null);
            }
            if (rec.getTrainingPlan() != null) {
                recMgr.addOrUpdateTextRecordValue(
                    acc.getId(), acc.getId(),
                    0, 15,
                    rec.getTrainingPlan(), params.getDay(), null);
            }
            if (rec.getAfterTrainingPulseRate() != null) {
                recMgr.addOrUpdateIntegerRecordValue(
                    acc.getId(), acc.getId(),
                    0, 16,
                    rec.getAfterTrainingPulseRate(), params.getDay(), null);
            }
            if (rec.getAfterTrainingWeight() != null) {
                recMgr.addOrUpdateFloatRecordValue(
                    acc.getId(), acc.getId(),
                    0, 17,
                    rec.getAfterTrainingWeight(), params.getDay(), null);
            }
            if (rec.getSweatingCodeId() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 18,
                    6, rec.getSweatingCodeId(), params.getDay(), null);
            }
            if (rec.getLoadHandlingCodeId() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    acc.getId(), acc.getId(),
                    0, 19,
                    6, rec.getLoadHandlingCodeId(), params.getDay(), null);
            }
            if (rec.getSportFood() != null) {
                recMgr.addOrUpdateTextRecordValue(
                    acc.getId(), acc.getId(),
                    0, 20,
                    rec.getSportFood(), params.getDay(), null);
            }
            if (rec.getDrugs() != null) {
                recMgr.addOrUpdateTextRecordValue(
                    acc.getId(), acc.getId(),
                    0, 21,
                    rec.getDrugs(), params.getDay(), null);
            }
            result.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            result.setSuccess(false);
        }
        return result;
    }

    @GET
    @Path("getCommonCoachRecords")
    @Produces(MediaType.APPLICATION_JSON)
    public GetCommonCoachRecordsResponse getCommonCoachRecords(
        @QueryParam("athleteId") Long athleteId,
        @QueryParam("day") String day) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        if (athleteId == null && getAccount() instanceof Athlete) {
            athleteId = getAccount().getId();
        }
        Date dayDate = null;
        try {
            dayDate = sdf.parse(day);
        } catch (ParseException ex) {
            throw new RuntimeException(ex);
        }
        RecordGroup group = recMgr.findRecordGroupByCode(1);
        List<RecordValue> values = recMgr.findRecordValues(group.getId(), athleteId, dayDate);
        CommonCoachRecords rec = new CommonCoachRecords();
        for (RecordValue val : values) {
            EnumRecordValue eval = (EnumRecordValue) val;
            switch (val.getRecord().getCode()) {
                case 0:
                    rec.setPresenceCodeId(eval.getValue().getCode());
                    break;
                case 1:
                    rec.setRateCodeId(eval.getValue().getCode());
                    break;
            }
        }
        GetCommonCoachRecordsResponse result = new GetCommonCoachRecordsResponse();
        result.setRecords(rec);
        result.setSuccess(true);
        return result;
    }

    @POST
    @Path("postCommonCoachRecords")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse postCommonCoachRecords(PostCommonCoachRecordsRequest params) {
        Account acc = getAccount();
        BasicResponse result = new BasicResponse();
        try {
            CommonCoachRecords rec = params.getRecords();
            if (rec.getPresenceCodeId() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    params.getAthleteId(), acc.getId(),
                    1, 0,
                    7, rec.getPresenceCodeId(),
                    params.getDay(), null);
            }
            if (rec.getRateCodeId() != null) {
                recMgr.addOrUpdateEnumRecordValue(
                    params.getAthleteId(), acc.getId(),
                    1, 1,
                    6, rec.getRateCodeId(),
                    params.getDay(), null);
            }
            result.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            result.setSuccess(false);
        }
        return result;
    }

    private GetAthleteDataResponse _getAthleteData(long athleteId) {
        Athlete athlete = athMgr.findAthlete(athleteId);
        DefaultAthleteData athleteData = new DefaultAthleteData();
        DefaultAccountData accData = new DefaultAccountData();
        GetAthleteDataResponse response = new GetAthleteDataResponse();
        DataMapper.map(athlete, accData);
        DataMapper.map(athlete, athleteData);
        response.setAthleteData(athleteData);
        response.setAccountData(accData);
        response.setSuccess(true);
        return response;
    }

    private GetCoachDataResponse _getCoachData(long coachId) {
        Coach coach = coachMgr.findCoach(coachId);
        DefaultCoachData coachData = new DefaultCoachData();
        DefaultAccountData accData = new DefaultAccountData();
        GetCoachDataResponse response = new GetCoachDataResponse();
        DataMapper.map(coach, accData);
        DataMapper.map(coach, coachData);
        response.setCoachData(coachData);
        response.setAccountData(accData);
        response.setSuccess(true);
        return response;
    }

    @GET
    @Path("getAthleteData")
    @Produces(MediaType.APPLICATION_JSON)
    public GetAthleteDataResponse getAthleteData() {
        return _getAthleteData(getAccount().getId());
    }

    @GET
    @Path("getCoachAthleteData")
    @Produces(MediaType.APPLICATION_JSON)
    public GetAthleteDataResponse getCoachAthleteData(
        @QueryParam("athleteId") long athleteId) {
        Coach coach = coachMgr.findCoach(getAccount().getId());
        if (!coach.getAttachedAthletes().stream().anyMatch(a -> a.getId() == athleteId)) {
            throw new IllegalStateException();
        }
        return _getAthleteData(athleteId);
    }

    @GET
    @Path("getCoachData")
    @Produces(MediaType.APPLICATION_JSON)
    public GetCoachDataResponse getCoachData() {
        return _getCoachData(getAccount().getId());
    }

    @GET
    @Path("getAthleteCoachData")
    @Produces(MediaType.APPLICATION_JSON)
    public GetCoachDataResponse getAthleteCoachData() {
        Athlete athlete = athMgr.findAthlete(getAccount().getId());
        return _getCoachData(athlete.getCoach().getId());
    }

    @POST
    @Path("updateAthleteData")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse updateAthleteData(UpdateAthleteDataRequest params) {
        BasicResponse result = new BasicResponse();
        try {
            Athlete athlete = athMgr.findAthlete(getAccount().getId());
            accMgr.updateAccountData(athlete.getId(), params.getAccountData());
            athMgr.updateAthleteData(athlete.getId(), params.getAthleteData());
            result.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            result.setSuccess(false);
        }
        return result;
    }

    @POST
    @Path("updateCoachData")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse updateCoachData(UpdateCoachDataRequest params) {
        BasicResponse result = new BasicResponse();
        try {
            Coach coach = coachMgr.findCoach(getAccount().getId());
            accMgr.updateAccountData(coach.getId(), params.getAccountData());
            coachMgr.updateCoachData(coach.getId(), params.getCoachData());
            result.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            result.setSuccess(false);
        }
        return result;
    }

    @GET
    @Path("getAthletes")
    @Produces(MediaType.APPLICATION_JSON)
    public GetAccountsResponse getAthletes() {
        GetAccountsResponse result = new GetAccountsResponse();

        List<AccountModel> accountModelsList = masterMgr
            .getAttachedAthletes(getAccount().getId())
            .stream()
            .map(a -> new AccountModel().fromAccount(a))
            .collect(Collectors.toList());
        result.setAccounts(accountModelsList);
        result.setSuccess(true);
        return result;
    }

    @GET
    @Path("getMasters")
    @Produces(MediaType.APPLICATION_JSON)
    public GetAccountsResponse getMasters() {
        GetAccountsResponse result = new GetAccountsResponse();

        List<AccountModel> accountModelsList = athMgr
            .findAthlete(getAccount().getId())
            .getMasters()
            .stream()
            .map(a -> new AccountModel().fromAccount(a))
            .collect(Collectors.toList());
        result.setAccounts(accountModelsList);
        result.setSuccess(true);
        return result;
    }

    @POST
    @Path("changePassword")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse changePassword(ChangePasswordRequest request) {
        BasicResponse result = new BasicResponse();
        try {
            accMgr.changePassword(getAccount().getId(), request.getOldPassword(), request.getNewPassword());
            result.setSuccess(true);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            result.setErrorMessage("Пароль указан неверно");
            result.setSuccess(false);
        }
        return result;
    }

    @GET
    @Path("getRequests")
    @Produces(MediaType.APPLICATION_JSON)
    public GetRequestsResponse getRequests() {
        GetRequestsResponse result = new GetRequestsResponse();

        List<RequestModel> requestsList = coachMgr
            .findCoach(getAccount().getId())
            .getRequests()
            .stream()
            .map(r -> new RequestModel(r.getId(), new AccountModel().fromAccount(r.getAthlete())))
            .collect(Collectors.toList());
        result.setRequests(requestsList);
        result.setSuccess(true);
        return result;
    }

    @POST
    @Path("acceptRequest")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse acceptRequest(@QueryParam("requestId") long requestId) {
        if (!coachMgr.findCoach(getAccount().getId()).getRequests().stream().anyMatch(r -> r.getId().equals(requestId))) {
            throw new IllegalStateException();
        }
        masterMgr.acceptAthleteRequest(requestId);
        return new BasicResponse(true);
    }

    @POST
    @Path("declineRequest")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse declineRequest(@QueryParam("requestId") long requestId) {
        if (!coachMgr.findCoach(getAccount().getId()).getRequests().stream().anyMatch(r -> r.getId().equals(requestId))) {
            throw new IllegalStateException();
        }
        masterMgr.declineAthleteRequest(requestId);
        return new BasicResponse(true);
    }

    @GET
    @Path("hasCoach")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse hasCoach() {
        return new BasicResponse(athMgr.findAthlete(getAccount().getId()).getCoach() != null);
    }

    @GET
    @Path("getAvailableCoaches")
    @Produces(MediaType.APPLICATION_JSON)
    public GetAvailableCoachesResponse getAvailableCoaches() {
        if (athMgr.findAthlete(getAccount().getId()).getCoach() != null) {
            throw new IllegalStateException();
        }

        GetAvailableCoachesResponse response = new GetAvailableCoachesResponse();
        try {
            response.setCoaches(coachMgr.findAllCoaches()
                .stream()
                .map(c -> new AccountModel().fromAccount(c))
                .collect(Collectors.toList()));
            response.setSuccess(true);
        } catch (Exception ex) {
        }
        return response;
    }

    @GET
    @Path("hasPendingRequest")
    @Produces(MediaType.APPLICATION_JSON)
    public HasPendingRequestResponse hasPendingRequest() {
        HasPendingRequestResponse response = new HasPendingRequestResponse();
        try {
            AthleteRequest req = athMgr.findAthleteRequest(getAccount().getId());
            response.setResult(req != null);
            if (req != null) {
                response.setCoach(new AccountModel().fromAccount(req.getMaster()));
            }
            response.setSuccess(true);
        } catch (Exception ex) {
            response.setSuccess(false);
        }
        return response;
    }

    @POST
    @Path("sendRequest")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse sendRequest(@QueryParam("coachId") long coachId) {
        if (athMgr.findAthleteRequest(getAccount().getId()) != null) {
            throw new IllegalStateException();
        }
        athMgr.createAthleteRequest(getAccount().getId(), coachId);
        return new BasicResponse(true);
    }

    @POST
    @Path("cancelRequest")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse cancelRequest() {
        AthleteRequest request = athMgr.findAthleteRequest(getAccount().getId());
        if (request == null) {
            throw new IllegalStateException();
        }
        athMgr.cancelAthleteRequest(request.getId());
        return new BasicResponse(true);
    }

    @GET
    @Path("getAvailableReceivers")
    @Produces(MediaType.APPLICATION_JSON)
    public GetAccountsResponse getAvailableReceivers() {
        GetAccountsResponse result = new GetAccountsResponse();
        try {
            result.setAccounts(
                msgMgr.getAvailableReceivers(getAccount().getId())
                .stream()
                .map(a -> new AccountModel().fromAccount(a))
                .collect(Collectors.toList())
            );
            result.setSuccess(true);
        } catch (Exception ex) {
        }
        return result;
    }

    @GET
    @Path("getMessagesStat")
    @Produces(MediaType.APPLICATION_JSON)
    public GetMessagesStat getMessagesStat(
        @QueryParam("idFrom") Long idFrom,
        @QueryParam("except") Long except) {
        GetMessagesStat result = new GetMessagesStat();
        List<DialogItem> list = msgMgr.findDialogItems(getAccount().getId(), idFrom, except);
        if (list.size() > 0) {
            result.setLastMessage(new MessageModel().fromMessage(list.get(0).getMessage()));
        }
        result.setLastId(msgMgr.findLastMessageId(getAccount().getId()));
        result.setCount(list.size());
        result.setSuccess(true);
        return result;
    }

    @POST
    @Path("registerAthlete")
    @Produces(MediaType.APPLICATION_JSON)
    public BasicResponse registerAthlete(RegisterAthleteRequest params) {
        BasicResponse result = new BasicResponse();
        try {
            if (accMgr.existsWithEmail(params.getEmail())) {
                result.setErrorMessage("Пользователь с таким email уже существует");
                result.setSuccess(false);
            } else {
                DefaultAthleteData data = new DefaultAthleteData();
                DefaultAccountData accData = new DefaultAccountData();
                Athlete athlete = athMgr.createAthlete(params.getEmail(), params.getPassword(), false, data);
                accData.setLastName(params.getLastName());
                accData.setFirstName(params.getFirstName());
                accData.setMiddleName(params.getMiddleName());
                accMgr.updateAccountData(athlete.getId(), accData);
                String token = accMgr.createOneTimeToken(athlete.getId());
                mailer.sendMail(
                    params.getEmail(),
                    "Подтверждение регистрации",
                    "Для подтверждения учетной записи перейдите по ссылке: http://localhost:8080/cr/w/approve?id=" + token);
                result.setSuccess(true);
            }
        } catch (Exception ex) {
            result.setErrorMessage("Ошибка создания пользователя");
        }
        return result;
    }
}
