package ru.bsu.coacher.web.rest;

import ru.bsu.coacher.shared.DefaultAccountData;
import ru.bsu.coacher.shared.DefaultCoachData;

public class UpdateCoachDataRequest {
    private DefaultCoachData coachData;
    private DefaultAccountData accountData;

    public DefaultCoachData getCoachData() {
        return coachData;
    }

    public void setCoachData(DefaultCoachData coachData) {
        this.coachData = coachData;
    }

    public DefaultAccountData getAccountData() {
        return accountData;
    }

    public void setAccountData(DefaultAccountData accountData) {
        this.accountData = accountData;
    }
    
}
