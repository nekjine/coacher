package ru.bsu.coacher.web.rest;

public class BasicResponse {
    private boolean success;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    
    public BasicResponse() {
        
    }
    
    public BasicResponse(boolean success) {
        this.success = success;
    }
}
