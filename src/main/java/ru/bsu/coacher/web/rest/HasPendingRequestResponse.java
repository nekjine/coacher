package ru.bsu.coacher.web.rest;

import java.io.Serializable;

public class HasPendingRequestResponse extends BasicResponse implements Serializable {
    private AccountModel coach;
    private Boolean result = false;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public AccountModel getCoach() {
        return coach;
    }

    public void setCoach(AccountModel coach) {
        this.coach = coach;
    }
}