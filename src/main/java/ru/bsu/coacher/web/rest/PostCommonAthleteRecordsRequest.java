package ru.bsu.coacher.web.rest;

import java.util.Date;

public class PostCommonAthleteRecordsRequest {
    private Date day;
    private CommonAthleteRecords records;

    public CommonAthleteRecords getRecords() {
        return records;
    }

    public void setRecords(CommonAthleteRecords records) {
        this.records = records;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }
}
