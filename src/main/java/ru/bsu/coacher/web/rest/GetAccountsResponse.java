package ru.bsu.coacher.web.rest;

import java.io.Serializable;
import java.util.List;

public class GetAccountsResponse extends BasicResponse implements Serializable {
    private List<AccountModel> accounts;

    public List<AccountModel> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountModel> accounts) {
        this.accounts = accounts;
    }

}