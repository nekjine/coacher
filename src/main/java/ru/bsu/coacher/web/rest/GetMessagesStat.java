package ru.bsu.coacher.web.rest;

import java.io.Serializable;

public class GetMessagesStat extends BasicResponse implements Serializable {
    private Integer count;
    private Long lastId;
    private MessageModel lastMessage;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getLastId() {
        return lastId;
    }

    public void setLastId(Long lastId) {
        this.lastId = lastId;
    }

    public MessageModel getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(MessageModel lastMessage) {
        this.lastMessage = lastMessage;
    }
}