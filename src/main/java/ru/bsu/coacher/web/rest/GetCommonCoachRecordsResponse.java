package ru.bsu.coacher.web.rest;

public class GetCommonCoachRecordsResponse extends BasicResponse {
    private CommonCoachRecords records;

    public CommonCoachRecords getRecords() {
        return records;
    }

    public void setRecords(CommonCoachRecords records) {
        this.records = records;
    }
}
