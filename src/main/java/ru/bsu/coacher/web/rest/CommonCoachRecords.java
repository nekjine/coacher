package ru.bsu.coacher.web.rest;

public class CommonCoachRecords {
    private Integer presenceCodeId;
    private Integer rateCodeId;

    public Integer getPresenceCodeId() {
        return presenceCodeId;
    }

    public void setPresenceCodeId(Integer presenceCodeId) {
        this.presenceCodeId = presenceCodeId;
    }

    public Integer getRateCodeId() {
        return rateCodeId;
    }

    public void setRateCodeId(Integer rateCodeId) {
        this.rateCodeId = rateCodeId;
    }
}
