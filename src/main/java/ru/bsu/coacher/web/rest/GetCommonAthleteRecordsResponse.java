package ru.bsu.coacher.web.rest;

public class GetCommonAthleteRecordsResponse extends BasicResponse {
    private CommonAthleteRecords records;

    public CommonAthleteRecords getRecords() {
        return records;
    }

    public void setRecords(CommonAthleteRecords records) {
        this.records = records;
    }
}
