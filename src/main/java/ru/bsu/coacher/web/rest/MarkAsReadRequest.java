package ru.bsu.coacher.web.rest;

public class MarkAsReadRequest {
    private Long[] ids;

    public Long[] getIds() {
        return ids;
    }

    public void setIds(Long[] ids) {
        this.ids = ids;
    }
}
