package ru.bsu.coacher.entities.records;

import javax.persistence.Entity;
import ru.bsu.coacher.shared.RecordValueType;

@Entity
public class FloatRecordValue extends RecordValue {
    private Float value;

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    @Override
    public RecordValueType getValueType() {
        return RecordValueType.Float;
    }
}
