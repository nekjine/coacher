package ru.bsu.coacher.entities.records;

import javax.persistence.Entity;
import ru.bsu.coacher.shared.RecordValueType;

@Entity
public class IntegerRecordValue extends RecordValue {
    private Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public RecordValueType getValueType() {
        return RecordValueType.Integer;
    }
}
