package ru.bsu.coacher.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import ru.bsu.coacher.shared.Gender;
import ru.bsu.coacher.shared.AccountData;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Account implements Serializable, AccountData {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date registered;
    
    @Column(nullable = false)
    private Boolean approvedEmail;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lockedUntil;
    
    @Column(nullable = false)
    private String passwordHash;
    
    @ManyToMany
    private final Collection<Role> roles = new ArrayList<>();
    
    @Column(unique = true, nullable = false)
    private String email;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
    private final Collection<OneTimeToken> oneTimeTokens = new ArrayList<>();
    
        @OneToMany(mappedBy = "owner")
    private final List<DialogItem> dialogItems = new ArrayList<>();
    
    @Enumerated(javax.persistence.EnumType.STRING)
    private Gender gender;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    
    private String workplace;
    private String occupation;
    
    private String phone;
    
    private String lastName;
    private String firstName;
    private String middleName;

    public List<DialogItem> getDialogItems() {
        return dialogItems;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getFullFIO() {
        if (lastName == null && firstName == null && middleName == null) {
            return "Безымянный #" + getId();
        }
        return lastName + " " + firstName + " " + middleName;
    }
    
    public String getShortFIO() {
        if (lastName == null && firstName == null && middleName == null) {
            return "Безымянный #" + getId();
        }
        return 
                (firstName == null || firstName.length() == 0 ? "" : firstName.substring(0, 1) + ". ") +
                (middleName == null || middleName.length() == 0 ? "" : middleName.substring(0, 1) + ". ") +
                lastName;
    }

    public Collection<OneTimeToken> getOneTimeTokens() {
        return oneTimeTokens;
    }

    public Collection<Role> getRoles() {
        return roles;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
    
    public boolean hasRole(String role) {
        for (Role r : roles) {
            if (r.getId().equals(role)) {
                return true;
            }
        }
        return false;
    }

    public Boolean getApprovedEmail() {
        return approvedEmail;
    }

    public void setApprovedEmail(Boolean approved) {
        this.approvedEmail = approved;
    }

    public Date getLockedUntil() {
        return lockedUntil;
    }

    public void setLockedUntil(Date lockedUntil) {
        this.lockedUntil = lockedUntil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    public String getType() {
        if (this instanceof Administrator) {
            return "Администратор";
        } else if (this instanceof Athlete) {
            return "Спортсмен";
        } else if (this instanceof Coach) {
            return "Тренер";
        } else if (this instanceof Medic) {
            return "Врач";
        } else if (this instanceof Psychologist) {
            return "Психолог";
        }
        return "Профиль";
    }

    @Override
    public String toString() {
        return "ru.bsu.coacher.Account[ id=" + id + " ]";
    }
}
