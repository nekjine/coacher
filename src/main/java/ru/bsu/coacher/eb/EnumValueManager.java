package ru.bsu.coacher.eb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ru.bsu.coacher.entities.EnumType;
import ru.bsu.coacher.entities.EnumValue;

@Stateless
public class EnumValueManager {
    @PersistenceContext
    private EntityManager em;
    
    public List<EnumValue> findAllEnumValues() {
        return em.createQuery("select v from EnumValue v", EnumValue.class).getResultList();
    }
    
    public List<EnumValue> findEnumValues(long enumTypeId) {
        return em.createQuery(
                "select v from EnumValue v where v.type.id = :eid", EnumValue.class)
                .setParameter("eid", enumTypeId)
                .getResultList();
    }
    
    public EnumValue findByCode(int enumTypeCode, int enumValueCode) {
        return em.createQuery(
        "select v from EnumValue v where v.code = :evCode and v.type.code = :etCode", EnumValue.class)
        .setParameter("etCode", enumTypeCode)
        .setParameter("evCode", enumValueCode)
        .getSingleResult();
    }
    
    public EnumValue create(long enumTypeId, String name, String comment) {
        EnumType type = em.find(EnumType.class, enumTypeId);
        EnumValue val = new EnumValue();
        val.setName(name);
        val.setDescription(comment);
        val.setType(type);
        type.getValues().add(val);
        em.persist(type);
        em.persist(val);
        return val;
    }
    
    public void delete(long enumValueId) {
        em.createQuery("delete from EnumValue e where e.id = :eid")
                .setParameter("eid", enumValueId)
                .executeUpdate();
    }
    
    public void update(long enumValueId, String name, String comment) {
        EnumValue val = em.find(EnumValue.class, enumValueId);
        val.setName(name);
        val.setDescription(comment);
    }
}
