package ru.bsu.coacher.eb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.AthleteRequest;
import ru.bsu.coacher.entities.Master;

@Stateless
public class MasterManager {
    @PersistenceContext
    private EntityManager em;
    
    public Master find(long masterId) {
        return em.find(Master.class, masterId);
    }
    
    public void attachAthlete(long masterId, long athleteId) {
        Master master = em.find(Master.class, masterId);
        Athlete athlete = em.find(Athlete.class, athleteId);
        master.getAttachedAthletes().add(athlete);
        em.persist(master);
    }
    
    public void detachAthlete(long masterId, final long athleteId) {
        Master spec = em.find(Master.class, masterId);
        spec.getAttachedAthletes().removeIf((Athlete t) -> t.getId() == athleteId);
        em.persist(spec);
    }
    
    public List<Athlete> getAttachedAthletes(long masterId) {
        return new ArrayList<>(
                em.find(Master.class, masterId).getAttachedAthletes());
    }
    
    public List<AthleteRequest> findAthleteRequests(long masterId) {
        return em
            .createQuery("select r from AthleteRequest r where r.master.id = :mid", AthleteRequest.class)
            .setParameter("mid", masterId)
            .getResultList();
    }
    
    public void acceptAthleteRequest(long requestId) {
        AthleteRequest request = em.find(AthleteRequest.class, requestId);
        Master master = request.getMaster();
        Athlete athlete = request.getAthlete();
        
        master.getAttachedAthletes().add(athlete);
        
        em.remove(request);
        em.persist(master);
    }
    
    public void declineAthleteRequest(long requestId) {
        AthleteRequest request = em.find(AthleteRequest.class, requestId);
        em.remove(request);
    }
}
