package ru.bsu.coacher.eb;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.AthleteRequest;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.entities.CompetitionResult;
import ru.bsu.coacher.entities.Master;
import ru.bsu.coacher.entities.Role;
import ru.bsu.coacher.shared.AthleteCompetitionResult;
import ru.bsu.coacher.shared.AthleteData;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultAthleteData;
import ru.bsu.coacher.shared.PasswordHasher;

@Stateless
public class AthleteManager {
    
    private static Logger logger = Logger.getLogger(
        AccountManager.class.getName());
    
    @PersistenceContext
    private EntityManager em;
    
    @EJB
    private AccountManager accMgr;
    
    public List<Athlete> findAllAthletes() {
        return em.createQuery("select a from Athlete a", Athlete.class).getResultList();
    }
    
    public Athlete findAthlete(long id) {
        return em.find(Athlete.class, id);
    }
    
    public List<Master> getMasters(long athleteId) {
        return new ArrayList<>(
                em.find(Athlete.class, athleteId).getMasters());
    }
    
    public Athlete createAthlete(String email, String password, boolean approved,
        DefaultAthleteData data) {
        Athlete athlete = new Athlete();
        DataMapper.map((AthleteData)data, athlete);
        athlete.setEmail(email);
        athlete.setPasswordHash(PasswordHasher.hash(password));
        athlete.setApprovedEmail(approved);
        
        athlete.getRoles().add(em.find(Role.class, "athlete"));
        em.persist(athlete);
        return athlete;
    }
    
    public void updateAthleteData(Long athleteId, DefaultAthleteData data) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        
        DataMapper.map((AthleteData)data, athlete);
        
        em.persist(athlete);
    }
    
    public void setCoach(long athleteId, long coachId) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        Coach coach = em.find(Coach.class, coachId);
        athlete.setCoach(coach);
        em.persist(athlete);
    }
    
    public void createAthleteRequest(long athleteId, long coachId) {
        AthleteRequest req = new AthleteRequest();
        Athlete athlete = em.find(Athlete.class, athleteId);
        Coach coach = em.find(Coach.class, coachId);
        Long results = em
            .createQuery("select count(r) from AthleteRequest r "
                + "where r.athlete.id = :athlete", Long.class)
            .setParameter("athlete", athleteId)
            .getSingleResult();
        if (results > 0) {
            throw new IllegalStateException("There is at least one request"
                + " from the athlete already");
        }
        req.setAthlete(athlete);
        req.setMaster(coach);
        em.persist(req);
    }
    
    public AthleteRequest findAthleteRequest(long athleteId) {
        List<AthleteRequest> list = em
            .createQuery("select r from AthleteRequest r where"
                + " r.athlete.id = :athlete", AthleteRequest.class)
            .setParameter("athlete", athleteId)
            .getResultList();
        if (list.isEmpty())
            return null;
        return list.get(0);
    }
    
    public int cancelAthleteRequest(long requestId) {
        return em
            .createQuery("delete from AthleteRequest r"
                + " where r.id = :requestId")
            .setParameter("requestId", requestId)
            .executeUpdate();
    }
    
    public void addCompetitionResult(long athleteId,
        AthleteCompetitionResult result) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        CompetitionResult cr = new CompetitionResult();
        cr.setAthlete(athlete);
        cr.setDate(result.getDate());
        cr.setName(result.getName());
        cr.setPlace(result.getPlace());
        cr.setRank(result.getRank());
        athlete.getCompetitionResults().add(cr);
        em.persist(athlete);
    }
    
    public int deleteCompetitionResult(long crId) {
        return em
            .createQuery("delete from CompetitionResult r "
                + "where r.id = :cid", CompetitionResult.class)
            .setParameter("cid", crId)
            .executeUpdate();
    }
}
