package ru.bsu.coacher.eb;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.OneTimeToken;
import ru.bsu.coacher.shared.AccountData;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.PasswordHasher;

@Stateless
public class AccountManager {

    private static Logger logger = Logger.getLogger(AccountManager.class.getName());

    @PersistenceContext
    private EntityManager em;
    
    public Account find(long accountId) {
        return em.find(Account.class, accountId);
    }

    public List<Account> findAll() {
        return em.createQuery("select e from Account e", Account.class).getResultList();
    }

    public void deleteAccount(long accountId) {
        Query q = em
                .createQuery("delete from Account a where a.id = :uid")
                .setParameter("uid", accountId);
        q.executeUpdate();
    }
    
    public void updateAccountData(long accountId, AccountData data) {
        Account account = em.find(Account.class, accountId);
        DataMapper.map(data, account);
        em.persist(account);
    }
    
    public Account findByEmail(String email) {
        return em
            .createQuery(
                "select a from Account a where a.email = :email",
                Account.class)
            .setParameter("email", email)
            .getSingleResult();
    }
    
    
    public boolean existsWithEmail(String email) {
        return em
            .createQuery(
                "select count(a) from Account a where a.email = :email",
                Long.class)
            .setParameter("email", email)
            .getSingleResult() > 0;
    }
    
    public void approve(long accountId) {
        Account account = em.find(Account.class, accountId);
        account.setApprovedEmail(Boolean.TRUE);
        em.persist(account);
    }
    
    public void approveByToken(String tokenId) {
        OneTimeToken token = em.find(OneTimeToken.class, tokenId);
        token.getAccount().setApprovedEmail(true);
        em.persist(token.getAccount());
        em.remove(token);
    }

    public void lock(Long accountId, Date dateUntil) {
        if (accountId == null) {
            throw new IllegalArgumentException();
        }
        Account account = em.find(Account.class, accountId);
        account.setLockedUntil(dateUntil);
        em.persist(account);
    }

    public void unlock(Long accountId) {
        if (accountId == null) {
            throw new IllegalArgumentException();
        }
        Account account = em.find(Account.class, accountId);
        account.setLockedUntil(null);
        em.persist(account);
    }
    
    public void changePassword(long accountId, String oldPassword, String newPassword) {
        Account account = em.find(Account.class, accountId);
        if (oldPassword != null) {
            if (!account.getPasswordHash().equals(PasswordHasher.hash(oldPassword))) {
                throw new IllegalStateException();
            }
        }
        account.setPasswordHash(PasswordHasher.hash(newPassword));
    }
    
    public String createOneTimeToken(long accountId) {
        UUID uuid = UUID.randomUUID();
        OneTimeToken ott = new OneTimeToken();
        ott.setAccount(em.find(Account.class, accountId));
        ott.setId(uuid.toString());
        em.persist(ott);
        return uuid.toString();
    }
}
