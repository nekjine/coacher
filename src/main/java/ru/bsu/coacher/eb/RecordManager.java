package ru.bsu.coacher.eb;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.EnumType;
import ru.bsu.coacher.entities.EnumValue;
import ru.bsu.coacher.entities.records.EnumRecordValue;
import ru.bsu.coacher.entities.records.FloatRecordValue;
import ru.bsu.coacher.entities.records.IntegerRecordValue;
import ru.bsu.coacher.entities.records.Record;
import ru.bsu.coacher.entities.records.RecordGroup;
import ru.bsu.coacher.entities.records.RecordValue;
import ru.bsu.coacher.entities.records.TextRecordValue;
import ru.bsu.coacher.shared.RecordPeriodicity;
import ru.bsu.coacher.shared.RecordSourceType;
import ru.bsu.coacher.shared.RecordValueType;

@Stateless
public class RecordManager {

    @PersistenceContext
    private EntityManager em;
    
    @EJB
    private EnumValueManager evMgr;

    public List<Record> findAllRecords() {
        return em.createQuery("select r from Record r", Record.class).getResultList();
    }

    public List<Record> findRecords(long groupId) {
        return em
        .createQuery("select r from Record r where r.group.id = :gid")
        .setParameter("gid", groupId)
        .getResultList();
    }

    public List<RecordValue> findRecordValues(long groupId) {
        return em
        .createQuery("select v from RecordValue v where v.record.group.id = :gid")
        .setParameter("gid", groupId)
        .getResultList();
    }

    public List<RecordValue> findRecordValues(long groupId, long athleteId, Date day) {
        return em.createQuery(
        "select v from RecordValue v where v.record.group.id = :gid and "
        + "v.day = :day and v.athlete.id = :aid",
        RecordValue.class)
        .setParameter("gid", groupId)
        .setParameter("day", new Date(day.getYear(), day.getMonth(), day.getDate()), TemporalType.DATE)
        .setParameter("aid", athleteId)
        .getResultList();
    }

    private RecordValue findRecordValue(long recordId, long athleteId, Date day) {
        List<RecordValue> results = em.createQuery(
        "select v from RecordValue v where v.record.id = :rid and "
        + "v.day = :day and v.athlete.id = :aid",
        RecordValue.class)
        .setParameter("rid", recordId)
        .setParameter("day", day, TemporalType.DATE)
        .setParameter("aid", athleteId)
        .getResultList();
        if (results.size() > 0) {
            return results.get(0);
        }
        return null;
    }
    
    public Record findRecordByCode(int recordGroupCode, int recordCode) {
        return em.createQuery(
        "select r from Record r where r.group.code = :gcode and r.code = :code", Record.class)
        .setParameter("gcode", recordGroupCode)
        .setParameter("code", recordCode)
        .getSingleResult();
    }

    public List<RecordGroup> findRecordGroups(RecordSourceType srcType) {
        return em
        .createQuery(
        "select g from RecordGroup g where g.sourceType = :srcType",
        RecordGroup.class)
        .setParameter("srcType", srcType)
        .getResultList();
    }

    public long addRecordGroup(String name, RecordSourceType sourceType,
    RecordPeriodicity periodicity, String comment) {
        RecordGroup group = new RecordGroup();
        group.setName(name);
        group.setSourceType(sourceType);
        group.setPeriodicity(periodicity);
        group.setComment(comment);
        em.persist(group);
        return group.getId();
    }

    public long addRecord(long groupId, String name, RecordValueType valueType,
    EnumType enumType,
    String comment) {
        Record rec = new Record();
        RecordGroup group = em.find(RecordGroup.class, groupId);
        rec.setName(name);
        rec.setGroup(group);
        rec.setValueType(valueType);
        rec.setComment(comment);
        rec.setEnumType(enumType);
        em.persist(rec);
        return rec.getId();
    }

    public long addRecord(long groupId, String name, RecordValueType valueType,
    Long enumTypeId, String comment) {
        EnumType enumType = enumTypeId == null ? null : em.find(EnumType.class, enumTypeId);
        return addRecord(groupId, name, valueType, enumType, comment);
    }

    private <T extends RecordValue> T processRecordValue(T value, Record record, Athlete athlete, String comment) {
        value.setRecord(record);
        value.setAthlete(athlete);
        value.setComment(comment);
        return value;
    }

    public long addTextRecordValue(long athleteId, long recordId, Date date, String value, String comment) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        Record rec = em.find(Record.class, recordId);
        TextRecordValue val = processRecordValue(new TextRecordValue(), rec, athlete, comment);
        val.setValue(value);
        val.setDay(date);
        val.setAdded(new Date());
        val.setUpdated(new Date());
        em.persist(val);
        return val.getId();
    }

    public void updateTextRecordValue(long recordId, String value, String comment) {
        TextRecordValue val = em.find(TextRecordValue.class, recordId);
        val.setValue(value);
        val.setComment(comment);
        val.setUpdated(new Date());
        em.persist(val);
    }

    public long addOrUpdateTextRecordValue(long athleteId, long authorId, int recordGroupCode, int recordCode, String value, Date date, String comment) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        Account author = em.find(Account.class, authorId);
        Record rec = findRecordByCode(recordGroupCode, recordCode);
        TextRecordValue val = (TextRecordValue) findRecordValue(rec.getId(), athleteId, date);
        if (val == null) {
            val = processRecordValue(new TextRecordValue(), rec, athlete, comment);
            val.setValue(value);
            val.setDay(date);
            val.setAdded(new Date());
            val.setAuthor(author);
        } else {
            val.setValue(value);
            val.setComment(comment);
        }
        val.setUpdated(new Date());
        em.persist(val);
        return val.getId();
    }

    public long addIntegerRecordValue(long athleteId, long recordId, Date date, Integer value, String comment) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        Record rec = em.find(Record.class, recordId);
        IntegerRecordValue val = processRecordValue(new IntegerRecordValue(), rec, athlete, comment);
        val.setValue(value);
        val.setDay(date);
        val.setAdded(new Date());
        val.setUpdated(new Date());
        em.persist(val);
        return val.getId();
    }

    public void updateIntegerRecordValue(long recordId, Integer value, String comment) {
        IntegerRecordValue val = em.find(IntegerRecordValue.class, recordId);
        val.setValue(value);
        val.setComment(comment);
        val.setUpdated(new Date());
        em.persist(val);
    }

    public long addOrUpdateIntegerRecordValue(long athleteId, long authorId, int recordGroupCode, int recordCode, Integer value, Date date, String comment) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        Account author = em.find(Account.class, authorId);
        Record rec = findRecordByCode(recordGroupCode, recordCode);
        IntegerRecordValue val = (IntegerRecordValue) findRecordValue(rec.getId(), athleteId, date);
        if (val == null) {
            val = processRecordValue(new IntegerRecordValue(), rec, athlete, comment);
            val.setValue(value);
            val.setDay(date);
            val.setAdded(new Date());
            val.setAuthor(author);
        } else {
            val.setValue(value);
            val.setComment(comment);
        }
        val.setUpdated(new Date());
        em.persist(val);
        return val.getId();
    }

    public long addFloatRecordValue(long athleteId, long recordId,  Date date, Float value,String comment) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        Record rec = em.find(Record.class, recordId);
        FloatRecordValue val = processRecordValue(new FloatRecordValue(), rec, athlete, comment);
        val.setValue(value);
        val.setDay(date);
        val.setAdded(new Date());
        val.setUpdated(new Date());
        em.persist(val);
        return val.getId();
    }

    public void updateFloatRecordValue(long recordId, Float value, String comment) {
        FloatRecordValue val = em.find(FloatRecordValue.class, recordId);
        val.setValue(value);
        val.setComment(comment);
        val.setUpdated(new Date());
        em.persist(val);
    }

    public long addOrUpdateFloatRecordValue(long athleteId, long authorId, int recordGroupCode,
    int recordCode, Float value, Date date, String comment) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        Account author = em.find(Account.class, authorId);
        Record rec = findRecordByCode(recordGroupCode, recordCode);
        FloatRecordValue val = (FloatRecordValue) findRecordValue(rec.getId(), athleteId, date);
        if (val == null) {
            val = processRecordValue(new FloatRecordValue(), rec, athlete, comment);
            val.setValue(value);
            val.setDay(date);
            val.setAdded(new Date());
            val.setAuthor(author);
        } else {
            val.setValue(value);
            val.setComment(comment);
        }
        val.setUpdated(new Date());
        em.persist(val);
        return val.getId();
    }

    public long addEnumRecordValue(long athleteId, long recordId,
    Date date, Long enumValueId, String comment) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        Record rec = em.find(Record.class, recordId);
        EnumValue enumVal = enumValueId == null ? null : em.find(EnumValue.class, enumValueId);
        EnumRecordValue val = processRecordValue(new EnumRecordValue(), rec, athlete, comment);
        val.setValue(enumVal);
        val.setDay(date);
        val.setAdded(new Date());
        val.setUpdated(new Date());
        em.persist(val);
        return val.getId();
    }

    public void updateEnumRecordValue(long recordId, Long enumValueId, String comment) {
        EnumRecordValue val = em.find(EnumRecordValue.class, recordId);
        EnumValue enumVal = enumValueId == null ? null : em.find(EnumValue.class, enumValueId);
        val.setValue(enumVal);
        val.setComment(comment);
        val.setUpdated(new Date());
        em.persist(val);
    }

    public long addOrUpdateEnumRecordValue(long athleteId, long authorId, int recordGroupCode,
    int recordCode, int enumTypeCode, int enumValueCode, Date date, String comment) {
        Athlete athlete = em.find(Athlete.class, athleteId);
        Account author = em.find(Account.class, authorId);
        Record rec = findRecordByCode(recordGroupCode, recordCode);
        EnumValue enumVal = evMgr.findByCode(enumTypeCode, enumValueCode);
        EnumRecordValue val = (EnumRecordValue)findRecordValue(rec.getId(), athleteId, date);
        if (val == null) {
            val = processRecordValue(new EnumRecordValue(), rec, athlete, comment);
            val.setValue(enumVal);
            val.setDay(date);
            val.setAdded(new Date());
            val.setAuthor(author);
        } else {
            val.setValue(enumVal);
            val.setComment(comment);
        }
        val.setUpdated(new Date());
        em.persist(val);
        return val.getId();
    }

    public int deleteRecordValue(long recordValueId) {
        return em
        .createQuery("delete from RecordValue v where v.id = :rid",
        RecordValue.class)
        .setParameter("rid", recordValueId)
        .executeUpdate();
    }

    public void deleteRecordGroup(long recordGroupId) {
        em.createQuery("delete from RecordGroup g where g.id = :gid and g.code is null")
        .setParameter("gid", recordGroupId)
        .executeUpdate();
    }

    public void updateRecordGroup(long groupId, String name, RecordSourceType sourceType,
    RecordPeriodicity periodicity, String comment) {
        RecordGroup group = em.find(RecordGroup.class, groupId);
        if (group.getCode() != null) {
            throw new IllegalStateException("Запрещено редактировать встроенную группу");
        }
        group.setName(name);
        group.setSourceType(sourceType);
        group.setPeriodicity(periodicity);
        group.setComment(comment);
        em.persist(group);
    }

    public long updateRecord(long recordId, String name, RecordValueType valueType,
    EnumType enumType,
    String comment) {

        Record rec = em.find(Record.class, recordId);
        if (rec.getCode() != null) {
            throw new IllegalStateException("Запрещено редактировать встроенную запись");
        }
        rec.setName(name);
        rec.setValueType(valueType);
        rec.setComment(comment);
        rec.setEnumType(enumType);
        em.persist(rec);
        return rec.getId();
    }

    public long updateRecord(long recordId, String name, RecordValueType valueType,
    Long enumTypeId, String comment) {
        EnumType enumType = enumTypeId == null ? null : em.find(EnumType.class, enumTypeId);
        return updateRecord(recordId, name, valueType, enumType, comment);
    }

    public void deleteRecord(long recordId) {
        em.createQuery("delete from Record r where r.id = :rid and r.code is null")
        .setParameter("rid", recordId)
        .executeUpdate();
    }

    public List<RecordGroup> findAllRecordGroups() {
        return em.createQuery("select g from RecordGroup g", RecordGroup.class)
        .getResultList();
    }

    public RecordGroup findRecordGroup(long id) {
        return em.find(RecordGroup.class, id);
    }

    public RecordGroup findRecordGroupByCode(int code) {
        return em
        .createQuery("select g from RecordGroup g where g.code = :code", RecordGroup.class)
        .setParameter("code", code)
        .getSingleResult();
    }
}
