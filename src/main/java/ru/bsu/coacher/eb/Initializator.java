package ru.bsu.coacher.eb;

import java.util.TimeZone;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ru.bsu.coacher.entities.Administrator;
import ru.bsu.coacher.entities.EnumType;
import ru.bsu.coacher.entities.EnumValue;
import ru.bsu.coacher.entities.Role;
import ru.bsu.coacher.entities.records.Record;
import ru.bsu.coacher.entities.records.RecordGroup;
import ru.bsu.coacher.shared.PasswordHasher;
import ru.bsu.coacher.shared.RecordPeriodicity;
import ru.bsu.coacher.shared.RecordSourceType;
import ru.bsu.coacher.shared.RecordValueType;

@Startup
@Singleton
public class Initializator {

    private static Logger logger = Logger.getLogger(Initializator.class.getName());

    @PersistenceContext
    EntityManager em;

    @EJB
    MessageManager msgMan;

    @EJB
    Mailer mailer;

    @PostConstruct
    public void onCreate() {
        if (em.find(Role.class, "admin") == null) {
            logger.info("Initializing roles");

            Role adminRole = new Role();
            adminRole.setId("admin");
            em.persist(adminRole);

            Role coachRole = new Role();
            coachRole.setId("coach");
            em.persist(coachRole);

            Role athleteRole = new Role();
            athleteRole.setId("athlete");
            em.persist(athleteRole);

            Role medicRole = new Role();
            medicRole.setId("medic");
            em.persist(medicRole);

            Role psychoRole = new Role();
            psychoRole.setId("psychologist");
            em.persist(psychoRole);

            Role masterRole = new Role();
            masterRole.setId("master");
            em.persist(masterRole);
            
            Administrator admin = new Administrator();
            admin.getRoles().add(adminRole);
            admin.setApprovedEmail(Boolean.TRUE);
            admin.setEmail("admin");
            admin.setPasswordHash(PasswordHasher.hash("admin"));
            admin.setFirstName("Администратор");
            em.persist(admin);

            // Встроенные перечисления
            // 4 степени
            EnumType enumType0 = new EnumType();
            enumType0.setName("4-степенная характеристика");
            enumType0.setCode(0);
            em.persist(enumType0);

            EnumValue enumValue00 = new EnumValue();
            enumValue00.setType(enumType0);
            enumValue00.setName("Плохо");
            enumValue00.setCode(0);
            em.persist(enumValue00);

            EnumValue enumValue01 = new EnumValue();
            enumValue01.setType(enumType0);
            enumValue01.setName("Удовлетворительно");
            enumValue01.setCode(1);
            em.persist(enumValue01);

            EnumValue enumValue02 = new EnumValue();
            enumValue02.setType(enumType0);
            enumValue02.setName("Хорошо");
            enumValue02.setCode(2);
            em.persist(enumValue02);

            EnumValue enumValue03 = new EnumValue();
            enumValue03.setType(enumType0);
            enumValue03.setName("Отлично");
            enumValue03.setCode(3);
            em.persist(enumValue03);

            // Тип жалоб
            EnumType enumType1 = new EnumType();
            enumType1.setName("Типы жалоб");
            enumType1.setCode(1);
            em.persist(enumType1);

            EnumValue enumValue10 = new EnumValue();
            enumValue10.setType(enumType1);
            enumValue10.setName("Нет");
            enumValue10.setCode(0);
            em.persist(enumValue10);

            EnumValue enumValue11 = new EnumValue();
            enumValue11.setType(enumType1);
            enumValue11.setName("Боли в мышцах");
            enumValue11.setCode(1);
            em.persist(enumValue11);

            EnumValue enumValue12 = new EnumValue();
            enumValue12.setType(enumType1);
            enumValue12.setName("Болит голова");
            enumValue12.setCode(2);
            em.persist(enumValue12);

            EnumValue enumValue13 = new EnumValue();
            enumValue13.setType(enumType1);
            enumValue13.setName("Температура");
            enumValue13.setCode(3);
            em.persist(enumValue13);

            EnumValue enumValue14 = new EnumValue();
            enumValue14.setType(enumType1);
            enumValue14.setName("Травма");
            enumValue14.setCode(4);
            em.persist(enumValue14);

            // 4 степени по высоте
            EnumType enumType2 = new EnumType();
            enumType2.setName("4-степенная характеристика (по высоте)");
            enumType2.setCode(2);
            em.persist(enumType2);

            EnumValue enumValue20 = new EnumValue();
            enumValue20.setType(enumType2);
            enumValue20.setName("Снижена");
            enumValue20.setCode(0);
            em.persist(enumValue20);

            EnumValue enumValue21 = new EnumValue();
            enumValue21.setType(enumType2);
            enumValue21.setName("Удовлетворительная");
            enumValue21.setCode(1);
            em.persist(enumValue21);

            EnumValue enumValue22 = new EnumValue();
            enumValue22.setType(enumType2);
            enumValue22.setName("Хорошая");
            enumValue22.setCode(2);
            em.persist(enumValue22);

            EnumValue enumValue23 = new EnumValue();
            enumValue23.setType(enumType2);
            enumValue23.setName("Высокая");
            enumValue23.setCode(3);
            em.persist(enumValue23);

            // Типы функций кишечника
            EnumType enumType3 = new EnumType();
            enumType3.setName("Типы функций кишечника");
            enumType3.setCode(3);
            em.persist(enumType3);

            EnumValue enumValue30 = new EnumValue();
            enumValue30.setType(enumType3);
            enumValue30.setName("Расстроена");
            enumValue30.setCode(0);
            em.persist(enumValue30);

            EnumValue enumValue31 = new EnumValue();
            enumValue31.setType(enumType3);
            enumValue31.setName("Не ясная");
            enumValue31.setCode(1);
            em.persist(enumValue31);

            EnumValue enumValue32 = new EnumValue();
            enumValue32.setType(enumType3);
            enumValue32.setName("Нормальная");
            enumValue32.setCode(2);
            em.persist(enumValue32);

            // Желание тренироваться
            EnumType enumType4 = new EnumType();
            enumType4.setName("Желание тренироваться");
            enumType4.setCode(4);
            em.persist(enumType4);

            EnumValue enumValue40 = new EnumValue();
            enumValue40.setType(enumType4);
            enumValue40.setName("Нет желания");
            enumValue40.setCode(0);
            em.persist(enumValue40);

            EnumValue enumValue41 = new EnumValue();
            enumValue41.setType(enumType4);
            enumValue41.setName("Среднее");
            enumValue41.setCode(1);
            em.persist(enumValue41);

            EnumValue enumValue42 = new EnumValue();
            enumValue42.setType(enumType4);
            enumValue42.setName("Большое");
            enumValue42.setCode(2);
            em.persist(enumValue42);

            // Режим предстоящей тренировки
            EnumType enumType5 = new EnumType();
            enumType5.setName("Режимы предстоящей тренировки");
            enumType5.setCode(5);
            em.persist(enumType5);

            EnumValue enumValue50 = new EnumValue();
            enumValue50.setType(enumType5);
            enumValue50.setName("Интенсивный");
            enumValue50.setCode(0);
            em.persist(enumValue50);

            EnumValue enumValue51 = new EnumValue();
            enumValue51.setType(enumType5);
            enumValue51.setName("Средний");
            enumValue51.setCode(1);
            em.persist(enumValue51);

            EnumValue enumValue52 = new EnumValue();
            enumValue52.setType(enumType5);
            enumValue52.setName("Разгрузочный (ОФП)");
            enumValue52.setCode(2);
            em.persist(enumValue52);

            EnumValue enumValue53 = new EnumValue();
            enumValue53.setType(enumType5);
            enumValue53.setName("Отдых");
            enumValue53.setCode(3);
            em.persist(enumValue53);

            // 3 степени
            EnumType enumType6 = new EnumType();
            enumType6.setName("3-степенная характеристика");
            enumType6.setCode(6);
            em.persist(enumType6);

            EnumValue enumValue60 = new EnumValue();
            enumValue60.setType(enumType6);
            enumValue60.setName("Плохо");
            enumValue60.setCode(0);
            em.persist(enumValue60);

            EnumValue enumValue61 = new EnumValue();
            enumValue61.setType(enumType6);
            enumValue61.setName("Среднее");
            enumValue61.setCode(1);
            em.persist(enumValue61);

            EnumValue enumValue62 = new EnumValue();
            enumValue62.setType(enumType6);
            enumValue62.setName("Хорошо");
            enumValue62.setCode(2);
            em.persist(enumValue62);
            
            // Посещение
            EnumType enumType7 = new EnumType();
            enumType7.setName("Статус посещения");
            enumType7.setCode(7);
            em.persist(enumType7);

            EnumValue enumValue70 = new EnumValue();
            enumValue70.setType(enumType7);
            enumValue70.setName("Был");
            enumValue70.setCode(0);
            em.persist(enumValue70);

            EnumValue enumValue71 = new EnumValue();
            enumValue71.setType(enumType7);
            enumValue71.setName("Не был");
            enumValue71.setCode(1);
            em.persist(enumValue71);

            // Встроенные группы
            {
                RecordGroup athleteGroup = new RecordGroup();
                athleteGroup.setPeriodicity(RecordPeriodicity.Day);
                athleteGroup.setCode(0);
                athleteGroup.setName("Оперативный контроль");
                athleteGroup.setSourceType(RecordSourceType.Athlete);
                em.persist(athleteGroup);

                Record record0 = new Record();
                record0.setCode(0);
                record0.setName("Пульс (ЧСС) утром лежа");
                record0.setValueType(RecordValueType.Integer);
                record0.setGroup(athleteGroup);
                record0.setComment("уд/мин");
                em.persist(record0);

                Record record1 = new Record();
                record1.setCode(1);
                record1.setName("Пульс (ЧСС) утром стоя");
                record1.setValueType(RecordValueType.Integer);
                record1.setGroup(athleteGroup);
                record1.setComment("уд/мин");
                em.persist(record1);

                Record record2 = new Record();
                record2.setCode(2);
                record2.setName("Нижнее артериальное давление");
                record2.setValueType(RecordValueType.Integer);
                record2.setGroup(athleteGroup);
                record2.setComment("мм. рт. ст.");
                em.persist(record2);

                Record record3 = new Record();
                record3.setCode(3);
                record3.setName("Верхнее артериальное давление");
                record3.setValueType(RecordValueType.Integer);
                record3.setGroup(athleteGroup);
                record3.setComment("мм. рт. ст.");
                em.persist(record3);

                Record record4 = new Record();
                record4.setCode(4);
                record4.setName("Сон");
                record4.setValueType(RecordValueType.Enum);
                record4.setEnumType(enumType0);
                record4.setGroup(athleteGroup);
                em.persist(record4);

                Record record5 = new Record();
                record5.setCode(5);
                record5.setName("Жалобы");
                record5.setValueType(RecordValueType.Enum);
                record5.setEnumType(enumType1);
                record5.setGroup(athleteGroup);
                em.persist(record5);

                Record record6 = new Record();
                record6.setCode(6);
                record6.setName("Настроение");
                record6.setValueType(RecordValueType.Enum);
                record6.setEnumType(enumType0);
                record6.setGroup(athleteGroup);
                em.persist(record6);

                Record record7 = new Record();
                record7.setCode(7);
                record7.setName("Самочувствие");
                record7.setValueType(RecordValueType.Enum);
                record7.setEnumType(enumType0);
                record7.setGroup(athleteGroup);
                em.persist(record7);

                Record record8 = new Record();
                record8.setCode(8);
                record8.setName("Работоспособность");
                record8.setValueType(RecordValueType.Enum);
                record8.setEnumType(enumType2);
                record8.setGroup(athleteGroup);
                em.persist(record8);

                Record record9 = new Record();
                record9.setCode(9);
                record9.setName("Аппетит");
                record9.setValueType(RecordValueType.Enum);
                record9.setEnumType(enumType0);
                record9.setGroup(athleteGroup);
                em.persist(record9);

                Record record10 = new Record();
                record10.setCode(10);
                record10.setName("Функция кишечника");
                record10.setValueType(RecordValueType.Enum);
                record10.setEnumType(enumType3);
                record10.setGroup(athleteGroup);
                em.persist(record10);

                Record record11 = new Record();
                record11.setCode(11);
                record11.setName("Желание тренироваться");
                record11.setValueType(RecordValueType.Enum);
                record11.setEnumType(enumType4);
                record11.setGroup(athleteGroup);
                em.persist(record11);

                Record record12 = new Record();
                record12.setCode(12);
                record12.setName("Масса тела до тренировки");
                record12.setValueType(RecordValueType.Float);
                record12.setGroup(athleteGroup);
                record12.setComment("кг");
                em.persist(record12);

                Record record13 = new Record();
                record13.setCode(13);
                record13.setName("Пульс (ЧСС) до тренировки");
                record13.setValueType(RecordValueType.Integer);
                record13.setGroup(athleteGroup);
                record13.setComment("уд/мин");
                em.persist(record13);

                Record record14 = new Record();
                record14.setCode(14);
                record14.setName("Режим предстоящей тренировки");
                record14.setValueType(RecordValueType.Enum);
                record14.setEnumType(enumType5);
                record14.setGroup(athleteGroup);
                em.persist(record14);

                Record record15 = new Record();
                record15.setCode(15);
                record15.setName("План предстоящей тренировки");
                record15.setValueType(RecordValueType.Text);
                record15.setGroup(athleteGroup);
                em.persist(record15);

                Record record16 = new Record();
                record16.setCode(16);
                record16.setName("Пульс (ЧСС) после тренировки");
                record16.setValueType(RecordValueType.Integer);
                record16.setGroup(athleteGroup);
                record16.setComment("уд/мин");
                em.persist(record16);

                Record record17 = new Record();
                record17.setCode(17);
                record17.setName("Масса тела после тренировки");
                record17.setValueType(RecordValueType.Float);
                record17.setGroup(athleteGroup);
                record17.setComment("кг");
                em.persist(record17);

                Record record18 = new Record();
                record18.setCode(18);
                record18.setName("Потоотделение");
                record18.setValueType(RecordValueType.Enum);
                record18.setEnumType(enumType6);
                record18.setGroup(athleteGroup);
                em.persist(record18);

                Record record19 = new Record();
                record19.setCode(19);
                record19.setName("Перенос нагрузки");
                record19.setValueType(RecordValueType.Enum);
                record19.setEnumType(enumType6);
                record19.setGroup(athleteGroup);
                em.persist(record19);

                Record record20 = new Record();
                record20.setCode(20);
                record20.setName("Спортивное питание");
                record20.setValueType(RecordValueType.Text);
                record20.setGroup(athleteGroup);
                em.persist(record20);

                Record record21 = new Record();
                record21.setCode(21);
                record21.setName("Препараты");
                record21.setValueType(RecordValueType.Text);
                record21.setGroup(athleteGroup);
                em.persist(record21);
            }
            {
                RecordGroup coachGroup = new RecordGroup();
                coachGroup.setCode(1);
                coachGroup.setName("Записи тренера");
                coachGroup.setPeriodicity(RecordPeriodicity.Day);
                coachGroup.setSourceType(RecordSourceType.Coach);
                em.persist(coachGroup);
                
                Record record0 = new Record();
                record0.setCode(0);
                record0.setName("Посещение");
                record0.setValueType(RecordValueType.Enum);
                record0.setEnumType(enumType7);
                record0.setGroup(coachGroup);
                em.persist(record0);
                
                Record record1 = new Record();
                record1.setCode(1);
                record1.setName("Оценка состояния");
                record1.setValueType(RecordValueType.Enum);
                record1.setEnumType(enumType6);
                record1.setGroup(coachGroup);
                em.persist(record1);
            }
        }
    }
}
