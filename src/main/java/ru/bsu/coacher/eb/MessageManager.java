package ru.bsu.coacher.eb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.DialogItem;
import ru.bsu.coacher.entities.Master;
import ru.bsu.coacher.entities.Message;
import ru.bsu.coacher.shared.LastMessage;

@Stateless
public class MessageManager {

    @PersistenceContext
    private EntityManager em;

    public void sendMessage(long senderId, long receiverId, String text) {
        Message message = new Message();
        Account sender = em.find(Account.class, senderId);
        Account receiver = em.find(Account.class, receiverId);
        message.setSender(sender);
        message.setReceiver(receiver);
        message.setText(text);
        message.setSent(new Date());
        message.setIsRead(Boolean.FALSE);

        DialogItem ownerItem = new DialogItem();
        ownerItem.setOwner(sender);
        ownerItem.setOpposite(receiver);
        ownerItem.setMessage(message);
        message.getDialogItems().add(ownerItem);

        DialogItem oppItem = new DialogItem();
        oppItem.setOwner(receiver);
        oppItem.setOpposite(sender);
        oppItem.setMessage(message);
        oppItem.setIsReceiver(true);
        message.getDialogItems().add(oppItem);

        em.persist(message);
    }

    public void setRead(long messageId) {
        Message message = em.find(Message.class, messageId);
        message.setIsRead(Boolean.TRUE);
        em.persist(message);
    }

    public List<LastMessage> findLastMessages(long participantId) {
        List results = em.createQuery("select i.opposite.id, max(i.message.id) as x, sum(case when i.message.isRead = false and i.isReceiver = true then 1 else 0 end), count(i) from DialogItem i "
            + "where i.owner.id = :oid group by i.opposite.id")
            .setParameter("oid", participantId)
            .getResultList();

        List<LastMessage> lastMessages = new ArrayList<>();
        for (Object _resultRow : results) {
            Object[] resultRow = (Object[]) _resultRow;
            long oppositeId = (long) resultRow[0];
            long maxMessageId = (long) resultRow[1];
            long unread = (long) resultRow[2];
            long total = (long) resultRow[3];

            Account account = em.find(Account.class, oppositeId);
            Message message = em.find(Message.class, maxMessageId);

            LastMessage item = new LastMessage();
            item.setAccountId(oppositeId);
            item.setAccountName(account.getFullFIO());
            item.setText(message.getText());
            item.setDate(message.getSent());
            item.setIsRead(message.getIsRead());
            item.setTotalUnread(unread);
            item.setTotal(total);
            lastMessages.add(item);
        }

        return lastMessages;
    }

    public List<DialogItem> findAllDialogItems(long ownerId, long oppositeId) {
        return em.createQuery("select d from DialogItem d where "
            + "d.owner.id = :own_id and d.opposite.id = :opp_id",
            DialogItem.class)
            .setParameter("own_id", ownerId)
            .setParameter("opp_id", oppositeId)
            .getResultList();
    }

    public List<DialogItem> findLastDialogItems(long ownerId, long oppositeId, int count) {
        return em.createQuery("select d from DialogItem d where "
            + "d.owner.id = :own_id and d.opposite.id = :opp_id "
            + "order by d.message.sent desc", DialogItem.class)
            .setParameter("own_id", ownerId)
            .setParameter("opp_id", oppositeId)
            .setMaxResults(count)
            .getResultList();
    }

    public List<DialogItem> findDialogItems(long ownerId, long idFrom, Long exceptId) {
        return em.createQuery("select d from DialogItem d where "
            + "d.owner.id = :own_id and d.message.id >= :id_from and d.opposite.id != :exc_id and d.message.isRead = false "
            + "order by d.message.id desc", DialogItem.class)
            .setParameter("own_id", ownerId)
            .setParameter("id_from", idFrom)
            .setParameter("exc_id", exceptId == null ? -1L : exceptId)
            .getResultList();
    }
    public Long findLastMessageId(long ownerId) {
        return em.createQuery("select max(d.message.id) from DialogItem d where "
            + "d.owner.id = :own_id and d.message.isRead = false", Long.class)
            .setParameter("own_id", ownerId)
            .getResultList().get(0);
    }

    public List<DialogItem> findDialogItems(
        long ownerId, long oppId, Long idFrom,
        Integer count, Date dateFrom, Date dateTo) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<DialogItem> query = builder.createQuery(DialogItem.class);

        Root<DialogItem> root = query.from(DialogItem.class);
        Join<DialogItem, Account> ownerJoin = root.join("owner");
        Join<DialogItem, Account> oppJoin = root.join("opposite");
        Join<DialogItem, Message> msgJoin = root.join("message");

        query.select(root);
        Collection<Predicate> preds = new ArrayList<>();

        preds.add(builder.and(builder.equal(ownerJoin.get("id"), ownerId)));
        preds.add(builder.and(builder.equal(oppJoin.get("id"), oppId)));
        if (dateFrom != null) {
            preds.add(builder.greaterThanOrEqualTo(msgJoin.<Date>get("sent"), dateFrom));
        }
        if (dateTo != null) {
            preds.add(builder.lessThanOrEqualTo(msgJoin.<Date>get("sent"), dateTo));
        }
        if (idFrom != null) {
            preds.add(builder.greaterThanOrEqualTo(msgJoin.<Long>get("id"), idFrom));
        }
        query.where(
            builder.and(
                preds.toArray(new Predicate[preds.size()])));
        query.orderBy(builder.desc(root.<Long>get("id")));
        TypedQuery<DialogItem> finalQuery = em.createQuery(query);
        if (count != null) {
            finalQuery.setMaxResults(count);
        }
        return finalQuery.getResultList();
    }

    public int markAsRead(Long receiverId, Long[] messageIds) {
        if (messageIds.length == 0) {
            return 0;
        }
        return em.createQuery(
            "update Message m set m.isRead = true where m.id in :id_list and m.receiver.id = :receiver_id and m.isRead = false")
            .setParameter("id_list", Arrays.asList(messageIds))
            .setParameter("receiver_id", receiverId)
            .executeUpdate();
    }

    public int markAllAsRead(Long receiverId) {
        return em.createQuery(
            "update Message m set m.isRead = true where m.receiver.id = :receiver_id")
            .setParameter("receiver_id", receiverId)
            .executeUpdate();
    }

    public Long getNonReadCount(Long receiverId) {
        return em.createQuery(
            "select count(m) from Message m where m.isRead = false and m.receiver.id = :receiver_id",
            Long.class)
            .setParameter("receiver_id", receiverId)
            .getSingleResult();
    }

    public Collection<Account> getAvailableReceivers(Long accountId) {
        Account account = em.find(Account.class, accountId);
        if (account instanceof Master) {
            Master master = (Master) account;
            return (List) master.getAttachedAthletes();
        } else if (account instanceof Athlete) {
            Athlete athlete = (Athlete) account;
            return (List) athlete.getMasters();
        }
        return new ArrayList<>();
    }
}
