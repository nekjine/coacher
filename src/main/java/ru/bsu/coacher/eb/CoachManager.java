package ru.bsu.coacher.eb;

import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.entities.Role;
import ru.bsu.coacher.shared.CoachData;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultCoachData;
import ru.bsu.coacher.shared.PasswordHasher;

@Stateless
public class CoachManager {
    private static Logger logger = Logger.getLogger(CoachManager.class.getName());
    
    @PersistenceContext
    private EntityManager em;
    
    @EJB
    private AccountManager accMgr;
    
    public List<Coach> findAllCoaches() {
        return em.createQuery("select c from Coach c", Coach.class).getResultList();
    }
    
    public Coach findCoach(long coachId) {
        return em.find(Coach.class, coachId);
    }
    
    public void updateCoachData(long coachId, DefaultCoachData coachData) {
        Coach coach = em.find(Coach.class, coachId);
        DataMapper.map((CoachData)coachData, coach);
        em.persist(coach);
    }
    
    public Coach createCoach(String email, String password, DefaultCoachData data) {
        Coach coach = new Coach();
        DataMapper.map((CoachData)data, coach);
        coach.setEmail(email);
        coach.setPasswordHash(PasswordHasher.hash(password));
        coach.setApprovedEmail(true);
        coach.getRoles().add(em.find(Role.class, "coach"));
        coach.getRoles().add(em.find(Role.class, "master"));
        em.persist(coach);
        return coach;
    }
}
