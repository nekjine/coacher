package ru.bsu.coacher.eb;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

@Stateless
public class Mailer {
    private Logger logger = Logger.getLogger(Mailer.class.getName());
    
    public void sendMail(String to, String subject, String text)  {
        HtmlEmail email = new HtmlEmail();
        email.setHostName("smtp.yandex.ru");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator("coacher.test", "bilbo90210"));
        email.setCharset("utf-8");
        email.setSSLOnConnect(true);
        try {
            email.setFrom("coacher.test@yandex.ru");
            email.setSubject(subject);
            email.setHtmlMsg(text);
            email.addTo(to);
            email.send();
        } catch (EmailException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }
}
